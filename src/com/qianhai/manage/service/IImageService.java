/**
 * 
 */
package com.qianhai.manage.service;

import java.util.List;
import java.util.Map;

import com.qianhai.manage.vo.ImageInfo;

/**
 * @author Administrator
 *
 */
public interface IImageService extends IDefaultService {
	
	public final static String BEAN_ID = "imageService";
	
	public ImageInfo selectImageInfo(String name);
	
	public List<ImageInfo> selectImageInfos(String pid);
	
	public List<ImageInfo> selectImageInfos(Map<String, Object> param, int page, int rows);
	
	public long countImageInfos(Map<String, Object> param);
	
	public void saveImageInfo(ImageInfo vo);
	
	public void updateImageInfo(ImageInfo vo);
	
	public void batchSaveImageInfos(List<ImageInfo> list);
	
	public void batchUpdateImageInfos(List<ImageInfo> list);
	
	public void deleteImageInfo(String cid);
	
	public void deleteImageInfos(String pid);

}
