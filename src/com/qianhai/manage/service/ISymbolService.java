/**
 * 
 */
package com.qianhai.manage.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.qianhai.manage.vo.WorkingDay;

/**
 * @author Administrator
 *
 */
public interface ISymbolService extends IDefaultService {
	
	public List<WorkingDay> queryWorkingDay(Map<String, Object> param);
	
	public void saveWorkingDay(WorkingDay vo);
	
	public void updateWorkingDay(WorkingDay vo);
	
	public void deleteWorkingDay(String id);
	
	public void workingDayInit(int year);
	
	public boolean selectWeekendHas(Date date);
	
	public void saveDateConf(Map<String, Object> param);
	
	public void saveDateConf(List<Map<String, Object>> list);
	
	public void deleteDateConf(int ndate);
	
	public void deleteDateConf(List<Integer> ndates);
	
	public boolean selectconfHas(int ndate);
	
	public void updateDateConf(Map<String, Object> param);
	
	public void updateDateConf(List<Map<String, Object>> list);

}
