/**
 * 
 */
package com.qianhai.manage.service;

import java.util.List;
import java.util.Map;

import com.qianhai.manage.vo.WaresInfo;

/**
 * @author Administrator
 *
 */
public interface IWaresService extends IDefaultService {
	
	public static final String BEAN_ID = "waresService";
	
	public List<WaresInfo> queryWares(Map<String, Object> param, int page, int rows);
	
	public Long countWares(Map<String, Object> param);
	
	public WaresInfo queryWares(Map<String, Object> param);
	
	public void saveWares(WaresInfo vo);
	
	public void updateWares(WaresInfo vo);
	
	public void deleteWares(String id);
	
	public void deleteWares(List<String> ids);
	
	public List<Map<String, Object>> queryWares2Users(Map<String, Object> param, int page, int rows);
	
	public Long countWares2Users(Map<String, Object> param);
	
	public Map<String, Object> queryWares2User(Map<String, Object> param);

}
