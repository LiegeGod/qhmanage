/**
 * 
 */
package com.qianhai.manage.service.impl;

import java.util.List;
import java.util.Map;

import com.qianhai.manage.service.IWaresService;
import com.qianhai.manage.vo.WaresInfo;

/**
 * @author Administrator
 *
 */
public class WaresServiceImpl extends DefaultServiceImpl implements
		IWaresService {

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IWaresService#queryWares(java.util.Map, int, int)
	 */
	@Override
	public List<WaresInfo> queryWares(Map<String, Object> param, int page,
			int rows) {
		String statement = this.getStatement("queryWares");
		return this.selectList(statement, param, page, rows);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IWaresService#countWares(java.util.Map)
	 */
	@Override
	public Long countWares(Map<String, Object> param) {
		String statement = this.getStatement("queryWares_count");
		Long count = (Long) this.getSqlSessionTemplate().selectOne(statement, param);
		return (count == null ? 0 : count);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IWaresService#queryWares(java.util.Map)
	 */
	@Override
	public WaresInfo queryWares(Map<String, Object> param) {
		List<WaresInfo> list = this.queryWares(param, 0, 0);
		WaresInfo info = new WaresInfo();
		if(null != list && !list.isEmpty()) {
			info = list.get(0);
		}
		return info;
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IWaresService#saveWares(com.qianhai.manage.vo.WaresInfo)
	 */
	@Override
	public void saveWares(WaresInfo vo) {
		String statement = this.getStatement("saveWares");
		vo.setId(this.getKeyGenerator().getUUID());
		this.save(statement, vo);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IWaresService#updateWares(com.qianhai.manage.vo.WaresInfo)
	 */
	@Override
	public void updateWares(WaresInfo vo) {
		String statement = this.getStatement("updateWares");
		this.update(statement, vo);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IWaresService#deleteWares(java.lang.String)
	 */
	@Override
	public void deleteWares(String id) {
		String statement = this.getStatement("deleteWares");
		this.getSqlSessionTemplate().delete(statement, id);
	}
	
	public void deleteWares(List<String> ids) {
		String statement = this.getStatement("deleteWares");
		this.batchUpdate(statement, ids);
	}
	
	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IWaresService#queryWares2Users(java.util.Map, int, int)
	 */
	@Override
	public List<Map<String, Object>> queryWares2Users(Map<String, Object> param, int page, int rows) {
		String statement = this.getStatement("queryWares2Users");
		return this.selectList(statement, param, page, rows);
	}
	
	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IWaresService#countWares2Users(java.util.Map)
	 */
	@Override
	public Long countWares2Users(Map<String, Object> param) {
		String statement = this.getStatement("queryWares2Users_count");
		Long count = (Long) this.getSqlSessionTemplate().selectOne(statement, param);
		return (count == null ? 0 : count);
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.qianhai.manage.service.IWaresService#queryWares2User(java.util.Map)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> queryWares2User(Map<String, Object> param) {
		String statement = this.getStatement("queryWares2User");
		Map<String, Object> result = (Map<String, Object>) this.getSqlSessionTemplate().selectOne(statement, param);
		return result;
	}

}
