/**
 * 
 */
package com.qianhai.manage.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.qianhai.manage.service.IUserService;
import com.qianhai.manage.vo.UserInfo;

/**
 * @author Administrator
 *
 */
public class UserServiceImpl extends DefaultServiceImpl implements
		IUserService {

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ILoginService#selectUserInfo(java.lang.String, java.lang.String)
	 */
	@Override
	public UserInfo selectUserInfo(String name, String pwd) {
		String statement = this.getStatement("selectUserInfo");
		Map<String, Object> parameter = new HashMap<String, Object>();
		parameter.put("username", name);
		parameter.put("password", pwd);
		UserInfo user = (UserInfo) this.getSqlSessionTemplate().selectOne(statement, parameter);
		return user;
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IUserService#selectUserInfos(java.util.Map, int, int)
	 */
	@Override
	public List<UserInfo> selectUserInfos(Map<String, Object> param, int page, int rows) {
		String statement = this.getStatement("selectUserInfo");
		return this.selectList(statement, param, page, rows);
	}
	
	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IUserService#countUserInfos(java.util.Map)
	 */
	@Override
	public long countUserInfos(Map<String, Object> param) {
		String statement = this.getStatement("selectUserInfo_count");
		Long count = (Long) this.getSqlSessionTemplate().selectOne(statement, param);
		return (count == null ? 0 : count);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ILoginService#saveUserInfo(com.qianhai.manage.vo.UserInfo)
	 */
	@Override
	public void saveUserInfo(UserInfo vo) {
		String statement = this.getStatement("saveUserInfo");
		this.save(statement, vo);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ILoginService#updateUserInfo(com.qianhai.manage.vo.UserInfo)
	 */
	@Override
	public void updateUserInfo(UserInfo vo) {
		String statement = this.getStatement("updateUserInfo");
		this.update(statement, vo);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ILoginService#deleteUserInfo(java.lang.String)
	 */
	@Override
	public void deleteUserInfo(String cid) {
		String statement = this.getStatement("deleteUserInfo");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("cid", cid);
		this.getSqlSessionTemplate().delete(statement, param);
	}

}
