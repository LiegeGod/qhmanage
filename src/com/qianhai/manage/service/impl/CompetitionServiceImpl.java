/**
 * 
 */
package com.qianhai.manage.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.qianhai.manage.service.ICompetitionService;
import com.qianhai.manage.vo.AwardPrizes;
import com.qianhai.manage.vo.CompetitionInfo;

/**
 * @author Administrator
 *
 */
public class CompetitionServiceImpl extends DefaultServiceImpl implements
		ICompetitionService {

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ICompetitionService#selectCompetitionInfos(java.util.Map, int, int)
	 */
	@Override
	public List<CompetitionInfo> selectCompetitionInfos(
			Map<String, Object> param, int page, int rows) {
		String statement = this.getStatement("selectCompetitionInfo");
		return this.selectList(statement, param, page, rows);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ICompetitionService#countCompetitionInfos(java.util.Map)
	 */
	@Override
	public long countCompetitionInfos(Map<String, Object> param) {
		String statement = this.getStatement("selectCompetitionInfo_count");
		Long count = (Long) this.getSqlSessionTemplate().selectOne(statement, param);
		return (count == null ? 0 : count);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ICompetitionService#saveCompetitionInfo(com.qianhai.manage.vo.CompetitionInfo)
	 */
	@Override
	public void saveCompetitionInfo(CompetitionInfo vo) {
		String statement = this.getStatement("saveCompetitionInfo");
		this.save(statement, vo);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ICompetitionService#updateCompetitionInfo(com.qianhai.manage.vo.CompetitionInfo)
	 */
	@Override
	public void updateCompetitionInfo(CompetitionInfo vo) {
		String statement = this.getStatement("updateCompetitionInfo");
		this.update(statement, vo);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ICompetitionService#deleteCompetitionInfo(java.util.List)
	 */
	@Override
	public void deleteCompetitionInfo(List<String> cids) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("cids", cids);
		this.deleteBy(param);
	}
	
	private void deleteBy(Map<String, Object> param) {
		String statement = this.getStatement("deleteBy");
		this.getSqlSessionTemplate().delete(statement, param);
	}

	@Override
	public List<AwardPrizes> queryPrizes(Map<String, Object> param, int page,
			int rows) {
		String statement = this.getStatement("queryPrizes");
		return this.selectList(statement, param, page, rows);
	}

	@Override
	public long countPrizes(Map<String, Object> param) {
		String statement = this.getStatement("queryPrizes_count");
		Long count = (Long) this.getSqlSessionTemplate().selectOne(statement, param);
		return (count == null ? 0 : count);
	}

	@Override
	public void updatePrizes(List<Map<String, Object>> ml) {
		String statement = this.getStatement("updatePrizes");
		this.batchUpdate(statement, ml);
	}

}
