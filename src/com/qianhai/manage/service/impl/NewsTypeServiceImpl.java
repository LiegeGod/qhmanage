/**
 * 
 */
package com.qianhai.manage.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.qianhai.manage.service.INewsTypeService;
import com.qianhai.manage.vo.NewsTypeInfo;

/**
 * @author Administrator
 *
 */
public class NewsTypeServiceImpl extends DefaultServiceImpl implements
		INewsTypeService {

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.INewsTypeService#selectNewsTypeInfos(java.util.Map, int, int)
	 */
	@Override
	public List<NewsTypeInfo> selectNewsTypeInfos(Map<String, Object> param,
			int pages, int rows) {
		String statement = this.getStatement("selectNewsTypeInfos");
		return this.selectList(statement, param, pages, rows);
	}
	
	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.INewsTypeService#countNewsTypeInfos(java.util.Map)
	 */
	@Override
	public long countNewsTypeInfos(Map<String, Object> param) {
		String statement = this.getStatement("selectNewsTypeInfos_count");
		Long count = (Long) this.getSqlSessionTemplate().selectOne(statement, param);
		return (count == null ? 0 : count);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.INewsTypeService#saveNewsTypeInfo(com.qianhai.manage.vo.NewsTypeInfo)
	 */
	@Override
	public void saveNewsTypeInfo(NewsTypeInfo vo) {
		String statement = this.getStatement("saveNewsTypeInfo");
		vo.setCid(this.getKeyGenerator().getUUID());
		this.save(statement, vo);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.INewsTypeService#updateNewsTypeInfo(com.qianhai.manage.vo.NewsTypeInfo)
	 */
	@Override
	public void updateNewsTypeInfo(NewsTypeInfo vo) {
		String statement = this.getStatement("updateNewsTypeInfo");
		this.update(statement, vo);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.INewsTypeService#deleteNewsTypeInfo(java.lang.String)
	 */
	@Override
	public void deleteNewsTypeInfo(String cid) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("cid", cid);
		this.deleteBy(param);
	}
	
	@Override
	public void deleteNewsTypeInfos(List<String> cids) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("cids", cids);
		this.deleteBy(param);
	}
	
	private void deleteBy(Map<String, Object> param) {
		String statement = this.getStatement("deleteBy");
		this.getSqlSessionTemplate().delete(statement, param);
	}
	
	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.INewsTypeService#selectSame(java.lang.Integer)
	 */
	@Override
	public boolean selectSame(Integer value) {
		String statement = this.getStatement("selectSame");
		Long count = (Long) this.getSqlSessionTemplate().selectOne(statement, value);
		return !(count == null || count == 0);
	}
	
	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.INewsTypeService#selectNewsTypeInfosByLevel(java.lang.String)
	 */
	@Override
	public List<NewsTypeInfo> selectNewsTypeInfosByLevel(String pid) {
		String statement = this.getStatement("selectNewsTypeInfos");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("pid", pid);
		param.put("sort", "value");
		return this.selectList(statement, param, 0, 0);
	}

}
