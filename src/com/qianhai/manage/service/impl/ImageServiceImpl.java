/**
 * 
 */
package com.qianhai.manage.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.qianhai.manage.service.IImageService;
import com.qianhai.manage.vo.ImageInfo;

/**
 * @author Administrator
 *
 */
public class ImageServiceImpl extends DefaultServiceImpl implements
		IImageService {

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IImageService#selectImageInfo(java.lang.String)
	 */
	@Override
	public ImageInfo selectImageInfo(String name) {
		String statement = this.getStatement("selectImageInfo");
		ImageInfo image = (ImageInfo) this.getSqlSessionTemplate().selectOne(statement, name);
		return image;
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IImageService#selectImageInfos(java.lang.String)
	 */
	@Override
	public List<ImageInfo> selectImageInfos(String pid) {
		String statement = this.getStatement("selectImageInfo");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("pid", pid);
		return this.selectList(statement, param, 0, 0);
	}
	
	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IImageService#selectImageInfos(java.util.Map, int, int)
	 */
	@Override
	public List<ImageInfo> selectImageInfos(Map<String, Object> param, int page, int rows) {
		String statement = this.getStatement("selectImageInfo");
		return this.selectList(statement, param, page, rows);
	}
	
	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IImageService#countImageInfos(java.util.Map)
	 */
	@Override
	public long countImageInfos(Map<String, Object> param) {
		String statement = this.getStatement("selectImageInfo_count");
		Long count = (Long) this.getSqlSessionTemplate().selectOne(statement, param);
		return (count == null ? 0 : count);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IImageService#saveImageInfo(com.qianhai.manage.vo.ImageInfo)
	 */
	@Override
	public void saveImageInfo(ImageInfo vo) {
		String statement = this.getStatement("saveImageInfo");
		vo.setCid(this.getKeyGenerator().getUUID());
		this.save(statement, vo);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IImageService#updateImageInfo(com.qianhai.manage.vo.ImageInfo)
	 */
	@Override
	public void updateImageInfo(ImageInfo vo) {
		String statement = this.getStatement("updateImageInfo");
		this.update(statement, vo);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IImageService#batchSaveImageInfos(java.util.List)
	 */
	@Override
	public void batchSaveImageInfos(List<ImageInfo> list) {
		String statement = this.getStatement("saveImageInfo");
		for(ImageInfo vo : list) {
			vo.setCid(this.getKeyGenerator().getUUID());
		}
		this.batchInsert(statement, list);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IImageService#batchUpdateImageInfos(java.util.List)
	 */
	@Override
	public void batchUpdateImageInfos(List<ImageInfo> list) {
		String statement = this.getStatement("updateImageInfo");
		this.batchUpdate(statement, list);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IImageService#deleteImageInfo(java.lang.String)
	 */
	@Override
	public void deleteImageInfo(String cid) {
		String statement = this.getStatement("deleteBy");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("cid", cid);
		this.getSqlSessionTemplate().delete(statement, param);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.IImageService#deleteImageInfos(java.lang.String)
	 */
	@Override
	public void deleteImageInfos(String pid) {
		String statement = this.getStatement("deleteBy");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("pid", pid);
		this.getSqlSessionTemplate().delete(statement, param);
	}

}
