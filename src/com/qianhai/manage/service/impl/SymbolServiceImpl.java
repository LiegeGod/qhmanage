/**
 * 
 */
package com.qianhai.manage.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.qianhai.manage.service.ISymbolService;
import com.qianhai.manage.util.DateUtil;
import com.qianhai.manage.vo.WorkingDay;

/**
 * @author Administrator
 *
 */
public class SymbolServiceImpl extends DefaultServiceImpl implements
		ISymbolService {
	
	private final static Logger log = LoggerFactory.getLogger(SymbolServiceImpl.class);

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ISymbolService#queryWorkingDay(java.util.Map)
	 */
	@Override
	public List<WorkingDay> queryWorkingDay(Map<String, Object> param) {
		String statement = this.getStatement("queryWorkingDay");
		return this.selectList(statement, param, 0, 0);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ISymbolService#saveWorkingDay(com.qianhai.manage.vo.WorkingDay)
	 */
	@Override
	public void saveWorkingDay(WorkingDay vo) {
		String statement = this.getStatement("saveWorkingDay");
		vo.setId(this.getKeyGenerator().getUUID());
		this.save(statement, vo);
		this.doSaveDateConf(vo);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ISymbolService#updateWorkingDay(com.qianhai.manage.vo.WorkingDay)
	 */
	@Override
	public void updateWorkingDay(WorkingDay vo) {
		String statement = this.getStatement("updateWorkingDay");
		this.doDeleteDateConf(vo);
		this.update(statement, vo);
		this.doSaveDateConf(vo);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ISymbolService#deleteWorkingDay(java.lang.String)
	 */
	@Override
	public void deleteWorkingDay(String id) {
		String statement = this.getStatement("deleteWorkingDay");
		WorkingDay vo = new WorkingDay();
		vo.setId(id);
		this.doDeleteDateConf(vo);
		this.getSqlSessionTemplate().delete(statement, id);
	}
	
	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ISymbolService#workingDayInit(int)
	 */
	@Override
	public void workingDayInit(int year) {
		List<Date> list = DateUtil.getWeekend(year);
		this.sworkingDayInit(year);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		for(Date date : list) {
			try {
				String str_date = df.format(date);
				date = df.parse(str_date);
			} catch(Exception e) {
				log.error("日期格式转换失败！");
			}
			if(!this.selectWeekendHas(date)) {
				WorkingDay day = new WorkingDay();
				day.setTitle("周末");
				day.setStartdate(date);
				c.setTime(date);
				c.add(Calendar.DAY_OF_MONTH, 1);
				Date enddate = c.getTime();
				day.setEnddate(enddate);
				this.saveWorkingDay(day);
			}
		}
	}
	
	private void sworkingDayInit(int year) {
		List<Date> list = DateUtil.getDay(year);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat df2 = new SimpleDateFormat("yyyyMMdd");
		List<Map<String, Object>> pl = new ArrayList<Map<String, Object>>();
		for(Date date : list) {
			int value = 0;
			try {
				String str_date = df.format(date);
				date = df.parse(str_date);
				String _val = df2.format(date);
				value = Integer.parseInt(_val);
			} catch(Exception e) {
				log.error("日期格式转换失败！");
			}
			if(!this.selectconfHas(value)) {
				//TODO----初始化工作日
				Map<String, Object> param = new HashMap<String, Object>();
				param.put("ndate", value);
				param.put("tradeFlag", 1);
				pl.add(param);
			}
		}
		this.saveDateConf(pl);
	}
	
	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ISymbolService#selectWeekendHas(java.util.Date)
	 */
	@Override
	public boolean selectWeekendHas(Date date) {
		String statement = this.getStatement("selectHas");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("date", date);
		Long count = (Long) this.getSqlSessionTemplate().selectOne(statement, param);
		long c = (count == null ? 0 : count);
		if(c > 0) {
			return true;
		} else {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ISymbolService#saveDateConf(java.util.Map)
	 */
	@Override
	public void saveDateConf(Map<String, Object> param) {
		String statement = this.getStatement("savedateconf");
		this.save(statement, param);
	}
	
	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ISymbolService#saveDateConf(java.util.List)
	 */
	@Override
	public void saveDateConf(List<Map<String, Object>> list) {
		String statement = this.getStatement("savedateconf");
		this.batchInsert(statement, list);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ISymbolService#deleteDateConf(int)
	 */
	@Override
	public void deleteDateConf(int ndate) {
		String statement = this.getStatement("deletedateconf");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("ndate", ndate);
		this.getSqlSessionTemplate().delete(statement, param);
	}
	
	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ISymbolService#deleteDateConf(java.util.List)
	 */
	@Override
	public void deleteDateConf(List<Integer> ndates) {
		String statement = this.getStatement("updatedateconf");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for(Integer ndate : ndates) {
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("ndate", ndate);
			param.put("tradeFlag", 1);
			list.add(param);
		}
		this.batchUpdate(statement, list);
	}
	
	private void doSaveDateConf(WorkingDay vo) {
		Date start = vo.getStartdate();
		Date end = vo.getEnddate();
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		List<Map<String, Object>> ul = new ArrayList<Map<String, Object>>();
		if(end == null) {
			Map<String, Object> param = new HashMap<String, Object>();
			String _val = df.format(start);
			int value = Integer.parseInt(_val);
			param.put("ndate", value);
			param.put("tradeFlag", 0);
			ul.add(param);
//			if(!this.selectconfHas(value)) {
//				this.saveDateConf(param);
//			}
			this.updateDateConf(ul);
		} else {
			Calendar c = Calendar.getInstance();
			for(int i=0; c.getTime().getTime() < end.getTime(); i++) {
				c.setTime(start);
				c.add(Calendar.DAY_OF_MONTH, i);
				Map<String, Object> param = new HashMap<String, Object>();
				String _val = df.format(c.getTime());
				int value = Integer.parseInt(_val);
				param.put("ndate", value);
				param.put("tradeFlag", 0);
				if(!this.selectconfHas(value)) {
					list.add(param);
				} else {
					ul.add(param);
				}
			}
			this.saveDateConf(list);
			this.updateDateConf(ul);
		}
	}
	
	private void doDeleteDateConf(WorkingDay vo) {
		String id = vo.getId();
		Map<String, Object> parameter = new HashMap<String, Object>();
		parameter.put("id", id);
		List<WorkingDay> wds = this.queryWorkingDay(parameter);
		if(wds != null && !wds.isEmpty()) {
			WorkingDay tmp = wds.get(0);
			Date start = tmp.getStartdate();
			Date end = tmp.getEnddate();
			DateFormat df = new SimpleDateFormat("yyyyMMdd");
			Calendar c = Calendar.getInstance();
			List<Integer> list = new ArrayList<Integer>();
			if(end == null) {
				c.setTime(start);
				if(!querySworkingDayHas(c.getTime(), id)) {
					String _val = df.format(start);
					int value = Integer.parseInt(_val);
					list.add(value);
				}
			} else {
				c.setTime(start);
				for(int i=0; c.getTime().getTime() < end.getTime(); i++) {
					c.setTime(start);
					c.add(Calendar.DAY_OF_MONTH, i);
					if(!querySworkingDayHas(c.getTime(), id)) {
						String _val = df.format(c.getTime());
						int value = Integer.parseInt(_val);
						list.add(value);
					}
				}
			}
			this.deleteDateConf(list);
		}
	}
	
	/**
	 * 是否存在有假期。
	 * @param date
	 * @return
	 */
	private boolean querySworkingDayHas(Date date, String id) {
		String statement = this.getStatement("selectInWorkingDay");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("date", date);
		param.put("id", id);
		Long count = (Long) this.getSqlSessionTemplate().selectOne(statement, param);
		if(count != null && count > 0) {
			return true;
		} else {
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ISymbolService#selectconfHas(int)
	 */
	@Override
	public boolean selectconfHas(int ndate) {
		String statement = this.getStatement("selectconfHas");
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("ndate", ndate);
		Long count = (Long) this.getSqlSessionTemplate().selectOne(statement, param);
		long c = (count == null ? 0 : count);
		return (c > 0);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ISymbolService#updateDateConf(java.util.Map)
	 */
	@Override
	public void updateDateConf(Map<String, Object> param) {
		String statement = this.getStatement("updatedateconf");
		this.update(statement, param);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.ISymbolService#updateDateConf(java.util.List)
	 */
	@Override
	public void updateDateConf(List<Map<String, Object>> list) {
		String statement = this.getStatement("updatedateconf");
		this.batchUpdate(statement, list);
	}

}
