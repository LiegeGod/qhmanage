/**
 * 
 */
package com.qianhai.manage.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.qianhai.manage.service.INewsService;
import com.qianhai.manage.vo.NewsInfo;

/**
 * @author Administrator
 *
 */
public class NewsServiceImpl extends DefaultServiceImpl implements INewsService {
	
	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.INewsService#selectNewsInfos(java.util.Map, int, int)
	 */
	@Override
	public List<NewsInfo> selectNewsInfos(Map<String, Object> param, int page, int rows) {
		String statement = this.getStatement("selectNewsInfos");
		return this.selectList(statement, param, page, rows);
	}
	
	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.INewsService#countNewsInfos(java.util.Map)
	 */
	@Override
	public long countNewsInfos(Map<String, Object> param) {
		String statement = this.getStatement("selectNewsInfos_count");
		Long count = (Long) this.getSqlSessionTemplate().selectOne(statement, param);
		return (count == null ? 0 : count);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.INewsService#saveNewsInfo(com.qianhai.manage.vo.NewsInfo)
	 */
	@Override
	public void saveNewsInfo(NewsInfo vo) {
		String statement = this.getStatement("saveNewsInfo");
		this.save(statement, vo);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.INewsService#updateNewsInfo(com.qianhai.manage.vo.NewsInfo)
	 */
	@Override
	public void updateNewsInfo(NewsInfo vo) {
		String statement = this.getStatement("updateNewsInfo");
		this.update(statement, vo);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.INewsService#deleteNewsInfo(java.lang.String)
	 */
	@Override
	public void deleteNewsInfo(String cid) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("cid", cid);
		this.deleteBy(param);
	}

	/* (non-Javadoc)
	 * @see com.qianhai.manage.service.INewsService#deleteNewsInfos(java.util.List)
	 */
	@Override
	public void deleteNewsInfos(List<String> cids) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("cids", cids);
		this.deleteBy(param);
	}
	
	private void deleteBy(Map<String, Object> param) {
		String statement = this.getStatement("deleteBy");
		this.getSqlSessionTemplate().delete(statement, param);
	}
	
}
