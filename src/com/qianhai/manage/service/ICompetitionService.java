/**
 * 
 */
package com.qianhai.manage.service;

import java.util.List;
import java.util.Map;

import com.qianhai.manage.vo.AwardPrizes;
import com.qianhai.manage.vo.CompetitionInfo;

/**
 * @author Administrator
 *
 */
public interface ICompetitionService extends IDefaultService {
	
	public final static String BEAN_ID = "competitionService";
	
	public List<CompetitionInfo> selectCompetitionInfos(Map<String, Object> param, int page, int rows);
	
	public long countCompetitionInfos(Map<String, Object> param);
	
	public void saveCompetitionInfo(CompetitionInfo vo);
	
	public void updateCompetitionInfo(CompetitionInfo vo);
	
	public void deleteCompetitionInfo(List<String> cids);
	
	public List<AwardPrizes> queryPrizes(Map<String, Object> param, int page, int rows);
	
	public long countPrizes(Map<String, Object> param);
	
	public void updatePrizes(List<Map<String, Object>> ml);

}
