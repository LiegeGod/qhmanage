/**
 * 
 */
package com.qianhai.manage.service;

import java.util.List;
import java.util.Map;

import com.qianhai.manage.vo.UserInfo;

/**
 * @author Administrator
 *
 */
public interface IUserService extends IDefaultService {
	
	public final static String BEAN_ID = "userService";
	
	public UserInfo selectUserInfo(String name, String pwd);
	
	public List<UserInfo> selectUserInfos(Map<String, Object> param, int page, int rows);
	
	public long countUserInfos(Map<String, Object> param);
	
	public void saveUserInfo(UserInfo vo);
	
	public void updateUserInfo(UserInfo vo);
	
	public void deleteUserInfo(String cid);

}
