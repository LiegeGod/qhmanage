/**
 * 
 */
package com.qianhai.manage.service;

import java.util.List;
import java.util.Map;

import com.qianhai.manage.vo.NewsInfo;

/**
 * @author Administrator
 *
 */
public interface INewsService extends IDefaultService {
	
	public final static String BEAN_ID = "newService";
	
	public List<NewsInfo> selectNewsInfos(Map<String, Object> param, int page, int rows);
	
	public long countNewsInfos(Map<String, Object> param);
	
	public void saveNewsInfo(NewsInfo vo);
	
	public void updateNewsInfo(NewsInfo vo);
	
	public void deleteNewsInfo(String cid);
	
	public void deleteNewsInfos(List<String> cids);

}
