/**
 * 
 */
package com.qianhai.manage.service;

import javax.sql.DataSource;

import org.mybatis.spring.SqlSessionTemplate;

/**
 * @author Administrator
 *
 */
public interface IDefaultService {
	
	public static final String BEAN_ID = "";
	
	public DataSource getDataSource() throws Exception;
	
	public SqlSessionTemplate getSqlSessionTemplate() throws Exception;

}
