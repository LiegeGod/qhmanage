/**
 * 
 */
package com.qianhai.manage.service;

import java.util.List;
import java.util.Map;

import com.qianhai.manage.vo.NewsTypeInfo;

/**
 * @author Administrator
 *
 */
public interface INewsTypeService extends IDefaultService {
	
	public final static String BEAN_ID = "newsTypeService";
	
	public List<NewsTypeInfo> selectNewsTypeInfos(Map<String, Object> param, int pages, int rows);
	
	public long countNewsTypeInfos(Map<String, Object> param);
	
	public void saveNewsTypeInfo(NewsTypeInfo vo);
	
	public void updateNewsTypeInfo(NewsTypeInfo vo);
	
	public void deleteNewsTypeInfo(String cid);
	
	public void deleteNewsTypeInfos(List<String> cids);
	
	public boolean selectSame(Integer value);
	
	public List<NewsTypeInfo> selectNewsTypeInfosByLevel(String pid);

}
