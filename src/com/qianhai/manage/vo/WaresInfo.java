/**
 * 
 */
package com.qianhai.manage.vo;

/**
 * @author Administrator
 *
 */
public class WaresInfo extends ValueObject {

	private static final long serialVersionUID = 3931863386380525159L;
	
	private String id;
	
	private String name;
	
	private Integer waresType;
	
	private Integer waresKind;
	
	private Integer goldType;
	
	private Double gold;
	
	private String beforeName;
	
	private String description;
	
	private String beforeDescription;
	
	private String sellingTime;
	
	private Integer duration;
	
	private Integer durationUnit;
	
	private Long num;
	
	private String picture1;
	
	private String picture2;
	
	private String picture3;
	
	private String picture4;
	
	private Integer randomFlag;
	
	private Double money;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the waresType
	 */
	public Integer getWaresType() {
		return waresType;
	}

	/**
	 * @param waresType the waresType to set
	 */
	public void setWaresType(Integer waresType) {
		this.waresType = waresType;
	}

	/**
	 * @return the waresKind
	 */
	public Integer getWaresKind() {
		return waresKind;
	}

	/**
	 * @param waresKind the waresKind to set
	 */
	public void setWaresKind(Integer waresKind) {
		this.waresKind = waresKind;
	}

	/**
	 * @return the goldType
	 */
	public Integer getGoldType() {
		return goldType;
	}

	/**
	 * @param goldType the goldType to set
	 */
	public void setGoldType(Integer goldType) {
		this.goldType = goldType;
	}

	/**
	 * @return the gold
	 */
	public Double getGold() {
		return gold;
	}

	/**
	 * @param gold the gold to set
	 */
	public void setGold(Double gold) {
		this.gold = gold;
	}

	/**
	 * @return the beforeName
	 */
	public String getBeforeName() {
		return beforeName;
	}

	/**
	 * @param beforeName the beforeName to set
	 */
	public void setBeforeName(String beforeName) {
		this.beforeName = beforeName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the beforeDescription
	 */
	public String getBeforeDescription() {
		return beforeDescription;
	}

	/**
	 * @param beforeDescription the beforeDescription to set
	 */
	public void setBeforeDescription(String beforeDescription) {
		this.beforeDescription = beforeDescription;
	}

	/**
	 * @return the sellingTime
	 */
	public String getSellingTime() {
		return sellingTime;
	}

	/**
	 * @param sellingTime the sellingTime to set
	 */
	public void setSellingTime(String sellingTime) {
		this.sellingTime = sellingTime;
	}

	/**
	 * @return the duration
	 */
	public Integer getDuration() {
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	/**
	 * @return the durationUnit
	 */
	public Integer getDurationUnit() {
		return durationUnit;
	}

	/**
	 * @param durationUnit the durationUnit to set
	 */
	public void setDurationUnit(Integer durationUnit) {
		this.durationUnit = durationUnit;
	}

	/**
	 * @return the num
	 */
	public Long getNum() {
		return num;
	}

	/**
	 * @param num the num to set
	 */
	public void setNum(Long num) {
		this.num = num;
	}

	/**
	 * @return the picture1
	 */
	public String getPicture1() {
		return picture1;
	}

	/**
	 * @param picture1 the picture1 to set
	 */
	public void setPicture1(String picture1) {
		this.picture1 = picture1;
	}

	/**
	 * @return the picture2
	 */
	public String getPicture2() {
		return picture2;
	}

	/**
	 * @param picture2 the picture2 to set
	 */
	public void setPicture2(String picture2) {
		this.picture2 = picture2;
	}

	/**
	 * @return the picture3
	 */
	public String getPicture3() {
		return picture3;
	}

	/**
	 * @param picture3 the picture3 to set
	 */
	public void setPicture3(String picture3) {
		this.picture3 = picture3;
	}

	/**
	 * @return the picture4
	 */
	public String getPicture4() {
		return picture4;
	}

	/**
	 * @param picture4 the picture4 to set
	 */
	public void setPicture4(String picture4) {
		this.picture4 = picture4;
	}

	/**
	 * @return the randomFlag
	 */
	public Integer getRandomFlag() {
		return randomFlag;
	}

	/**
	 * @param randomFlag the randomFlag to set
	 */
	public void setRandomFlag(Integer randomFlag) {
		this.randomFlag = randomFlag;
	}

	/**
	 * @return the money
	 */
	public Double getMoney() {
		return money;
	}

	/**
	 * @param money the money to set
	 */
	public void setMoney(Double money) {
		this.money = money;
	}

}
