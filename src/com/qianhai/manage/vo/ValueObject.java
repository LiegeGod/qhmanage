/**
 * 
 */
package com.qianhai.manage.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Administrator
 *
 */
public class ValueObject implements Serializable {

	private static final long serialVersionUID = 2230424712719203921L;
	
	private String cid;
	
	private String creator;
	
	private String modifiedPerson;
	
	private Date createTime;
	
	private Date modifyTime;

	/**
	 * @return the cid
	 */
	public String getCid() {
		return cid;
	}

	/**
	 * @param cid the cid to set
	 */
	public void setCid(String cid) {
		this.cid = cid;
	}

	/**
	 * @return the creator
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * @param creator the creator to set
	 */
	public void setCreator(String creator) {
		this.creator = creator;
	}

	/**
	 * @return the modifiedPerson
	 */
	public String getModifiedPerson() {
		return modifiedPerson;
	}

	/**
	 * @param modifiedPerson the modifiedPerson to set
	 */
	public void setModifiedPerson(String modifiedPerson) {
		this.modifiedPerson = modifiedPerson;
	}

	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return the modifyTime
	 */
	public Date getModifyTime() {
		return modifyTime;
	}

	/**
	 * @param modifyTime the modifyTime to set
	 */
	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}
	
}
