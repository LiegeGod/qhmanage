/**
 * 
 */
package com.qianhai.manage.vo;

import java.util.List;

/**
 * @author Administrator
 *
 */
public class NewsInfo extends ValueObject {

	private static final long serialVersionUID = 5264276647618044911L;
	
	private String title;
	
	private String content;
	
	private String marks;
	
	private String url;
	
	private String path;
	
	private String type;
	
	private String levelt;
	
	private List<ImageInfo> images;
	
	private String typename;
	
	private String levelname;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the marks
	 */
	public String getMarks() {
		return marks;
	}

	/**
	 * @param marks the marks to set
	 */
	public void setMarks(String marks) {
		this.marks = marks;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the levelt
	 */
	public String getLevelt() {
		return levelt;
	}

	/**
	 * @param levelt the levelt to set
	 */
	public void setLevelt(String levelt) {
		this.levelt = levelt;
	}

	/**
	 * @return the images
	 */
	public List<ImageInfo> getImages() {
		return images;
	}

	/**
	 * @param images the images to set
	 */
	public void setImages(List<ImageInfo> images) {
		this.images = images;
	}

	/**
	 * @return the typename
	 */
	public String getTypename() {
		return typename;
	}

	/**
	 * @param typename the typename to set
	 */
	public void setTypename(String typename) {
		this.typename = typename;
	}

	/**
	 * @return the levelname
	 */
	public String getLevelname() {
		return levelname;
	}

	/**
	 * @param levelname the levelname to set
	 */
	public void setLevelname(String levelname) {
		this.levelname = levelname;
	}

}
