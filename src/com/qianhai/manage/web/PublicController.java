/**
 * 
 */
package com.qianhai.manage.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qianhai.manage.service.INewsService;
import com.qianhai.manage.service.IUserService;
import com.qianhai.manage.util.OperationUtil;
import com.qianhai.manage.util.StringUtil;
import com.qianhai.manage.vo.NewsInfo;
import com.qianhai.manage.vo.UserInfo;

/**
 * @author Administrator
 *
 */
@Controller
@RequestMapping("public")
public class PublicController extends BaseController {
	
	@Resource
	private INewsService newsService;
	
	@Resource
	private IUserService userService;
	
	@RequestMapping("banner/shownews")
	public String toShowNews(String key, ModelMap map) {
		key = key.replace("{XX}", "");
		String value = OperationUtil.decode(key);
		if(value.indexOf("[&%&]") == -1) {
			map.put("msg", "参数有误！");
			return "error";
		}
		String[] strs = value.split("\\[\\&\\%\\&\\]");
		if(!"qhym".equals(strs[1])) {
			map.put("msg", "参数有误！");
			return "error";
		}
		String cid = strs[0];
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("cid", cid);
		List<NewsInfo> list = this.newsService.selectNewsInfos(param, 0, 0);
		if(list == null || list.isEmpty()) {
			map.put("msg", "参数有误！");
			return "error";
		}
		NewsInfo vo = list.get(0);
		map.put("vo", vo);
		return "news/shownews";
	}
	
	@ResponseBody
	@RequestMapping("login")
	public Map<String, Object> login(HttpServletRequest request, UserInfo user) {
		Map<String, Object> result = new HashMap<String, Object>();
		if(StringUtil.isEmpty(user.getUsername()) 
				|| StringUtil.isEmpty(user.getPassword())) {
			result.put("result", "请完整登录信息！");
			return result;
		}
		String name = user.getUsername();
		String pwd = user.getPassword();
		UserInfo _user = this.userService.selectUserInfo(name, pwd);
		if(_user == null || StringUtil.isEmpty(_user.getCid())) {
			result.put("result", "登录出错，请确认登录名和密码是否一致！");
			return result;
		}
		HttpSession session = request.getSession();
		session.setAttribute("username", _user.getUsername());
		String value = name + "[&%&]" + "qhym";
		String _value = OperationUtil.encode(value);
		session.setAttribute("user_validate", _value);
		session.setAttribute("user_level", user.getLevel());
		result.put("result", 1);
		return result;
	}
	
	@RequestMapping("logout")
	public String logout(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("username", "");
		session.setAttribute("user_validate", "");
		session.setAttribute("user_level", "");
		return "redirect:/login.jsp";
	}
	
	@ResponseBody
	@RequestMapping("getBanner")
	public Map<String, Object> getBanner(HttpServletRequest request) {
		Map<String, Object> result = new HashMap<String, Object>();
//		int page = this.getPage(request);
//		int rows = this.getRows(request);
//		Map<String, Object> param = new HashMap<String, Object>();
//		param.put("levelt", 1);
//		param.put("sort", "create_time desc");
//		List<NewsInfo> list =  this.newsService.selectNewsInfos(param, page, rows);
//		result.put("list", list);
		return result;
	}

}
