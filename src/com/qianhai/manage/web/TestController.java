/**
 * 
 */
package com.qianhai.manage.web;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Administrator
 *
 */
@Controller
public class TestController extends BaseController {
	
	@RequestMapping("test")
	public String init(HttpServletRequest request, ModelMap map) {
		System.out.println("_______________TEST______________");
		map.put("test", "My test!!!!");
		return "test";
	}

}
