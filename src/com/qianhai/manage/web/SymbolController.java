/**
 * 
 */
package com.qianhai.manage.web;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qianhai.manage.service.ISymbolService;
import com.qianhai.manage.util.StringUtil;
import com.qianhai.manage.vo.WorkingDay;

/**
 * @author Administrator
 *
 */
@Controller
@RequestMapping("symbol")
public class SymbolController extends BaseController {
	
	@Resource
	private ISymbolService symbolService;
	
	@RequestMapping("workdayinit")
	public String workdayInit() {
		return "symbol/workday";
	}
	
	@RequestMapping("selecttimeinit")
	public String selecttimeInit(String action, String date, String id, ModelMap map) {
		if("1".equals(action)) {
			map.put("startdate", date);
		} else {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("id", id);
			WorkingDay wd = new WorkingDay();
			List<WorkingDay> wds = this.symbolService.queryWorkingDay(param);
			if(wds != null && !wds.isEmpty()) {
				wd = wds.get(0);
			}
			map.put("vo", wd);
			map.put("startdate", df.format(wd.getStartdate()));
			String enddate = "";
			if(wd.getEnddate() != null) {
				enddate = df.format(wd.getEnddate());
			}
			map.put("enddate", enddate);
		}
		map.put("action", action);
		return "symbol/selecttime";
	}
	
	@ResponseBody
	@RequestMapping("getEventData")
	public List<WorkingDay> getEventData(String start, String end) {
		List<WorkingDay> data = new ArrayList<WorkingDay>();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Map<String, Object> param = new HashMap<String, Object>();
		try {
			param.put("startdate", df.parse(start));
			param.put("enddate", df.parse(end));
		} catch (ParseException e) {
			param.clear();
		}
		if(!StringUtil.isEmpty(end) && end.indexOf("-") != -1) {
			String[] days = end.split("-");
			int year = Integer.parseInt(days[0]);
			this.symbolService.workingDayInit(year);
		}
		data = this.symbolService.queryWorkingDay(param);
		return data;
	}
	
	@ResponseBody
	@RequestMapping("saveworkday")
	public Map<String, Object> saveworkday(HttpServletRequest request, WorkingDay vo) {
		Map<String, Object> result = new HashMap<String, Object>();
		if(StringUtil.isEmpty(vo.getId())) {
			this.symbolService.saveWorkingDay(vo);
		} else {
			this.symbolService.updateWorkingDay(vo);
		}
		result.put("data", "OK");
		return result;
	}
	
	@ResponseBody
	@RequestMapping("deleteworkday")
	public Map<String, Object> deleteworkday(HttpServletRequest request, String id) {
		Map<String, Object> result = new HashMap<String, Object>();
		if(StringUtil.isEmpty(id)) {
			result.put("msg", "没有找到对应的数据。");
			return result;
		}
		this.symbolService.deleteWorkingDay(id);
		result.put("msg", "1");
		return result;
	}

}
