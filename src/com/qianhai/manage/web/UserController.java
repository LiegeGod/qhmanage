/**
 * 
 */
package com.qianhai.manage.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qianhai.manage.service.IUserService;
import com.qianhai.manage.util.TransformationUtil;
import com.qianhai.manage.vo.UserInfo;

/**
 * @author Administrator
 *
 */
@Controller
@RequestMapping("user")
public class UserController extends BaseController {
	
	@Resource
	private IUserService userService;
	
	@RequestMapping("usermanage")
	public String toUserManage() {
		return "";
	}
	
	@ResponseBody
	@RequestMapping("queryUserInfos")
	public Map<String, Object> queryUserInfos(HttpServletRequest request, UserInfo user) {
		Map<String, Object> result = new HashMap<String, Object>();
		int page = this.getPage(request);
		int rows = this.getRows(request);
		Map<String, Object> param = TransformationUtil.Object2HashMap(user);
		param.put("level", user.getLevel());
		List<UserInfo> list = this.userService.selectUserInfos(param, page, rows);
		long count = this.userService.countUserInfos(param);
		result.put("rows", list);
		result.put("total", count);
		return result;
	}

}
