/**
 * 
 */
package com.qianhai.manage.web;

import javax.servlet.http.HttpServletRequest;

import com.qianhai.manage.util.StringUtil;

/**
 * <pre>
 * 程序的中文名称。
 * </pre>
 * @author lijiawei  lijiawei@foresee.cn
 * @version 1.00.00
 * <pre>
 * 修改记录
 *    修改后版本:     修改人：  修改日期:     修改内容: 
 * </pre>
 */
public class BaseController {
	
	protected int getPage(HttpServletRequest request) {
		int page = Integer.parseInt(StringUtil.addDefault(request.getParameter("page"), "1"));
		if(page < 1) {
			page = 1;
		}
		return page;
	}
	
	protected int getRows(HttpServletRequest request) {
		int rows = Integer.parseInt(StringUtil.addDefault(request.getParameter("rows"), "10"));
		return rows;
	}

}
