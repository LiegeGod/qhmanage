/**
 * 
 */
package com.qianhai.manage.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * @author Administrator
 *
 */
public class TransformationUtil {
	
	public static void copyProperties(Object source, Object target) {
		JSONObject jsonObject = JSONObject.fromObject(source);
		target = JSONObject.toBean(jsonObject, target.getClass());
	}
	
	@SuppressWarnings("unchecked")
	public static <K, V> Map<K, V> Object2HashMap(Object object) {
		Map<K, V> map = new HashMap<K, V>();
		if(null != object) {
			JSONObject jsonObject = JSONObject.fromObject(object);
			for(Object key : jsonObject.keySet()) {
				Object value = jsonObject.get(key);
				map.put((K) key, (V) value);
			}
		}
		return map;
	}
	
	/**
	 * 替换MAP里面的KEY值。
	 * @param keyMap Map<K 原来的KEY值, K 替换为的KEY值>
	 * @param srcMap Map<K, V> MAP数据源
	 * @return Map<K, V>
	 */
	public static <K, V> Map<K, V> changeMapKey(Map<K, K> keyMap, Map<K, V> srcMap) {
		Map<K, V> map = new HashMap<K, V>();
		for(K key : keyMap.keySet()) {
			K value = keyMap.get(key);
			if(srcMap.containsKey(key)) {
				V val = srcMap.get(key);
				map.put(value, val);
			}
		}
		return map;
	}
	
	@SuppressWarnings("unchecked")
	public static <K, V> Map<K, V> JSONString2HashMap(String JSONString) {
		Map<K, V> map = new HashMap<K, V>();
		if(JSONString != null && !"".equals(JSONString)) {
			JSONString = JSONString.replace("[", "").replace("]", "");
			JSONObject jsonObject = JSONObject.fromObject(JSONString);
			for(Object key : jsonObject.keySet()) {
				Object value = jsonObject.get(key);
				map.put((K) key, (V) value);
			}
		}
		return map;
	}
	
	public static <K, V> List<Map<K, V>> JSONString2ArrayList(String JSONString) {
		List<Map<K, V>> list = new ArrayList<Map<K,V>>();
		if(JSONString != null && !"".equals(JSONString)) {
			JSONArray array = JSONArray.fromObject(JSONString);
			for(Object object : array) {
				Map<K, V> map = Object2HashMap(object);
				list.add(map);
			}
		}
		return list;
	}
	
	/**
	 * 转换为boolean
	 */
	public static boolean parseBoolean(Object obj){
		boolean bool = false;
		if(obj instanceof Double){
			Double dbobj = (Double)obj;
			if(dbobj.intValue() == 1) {
				bool = true;
			}
		}else if(obj instanceof Byte){
			Byte btobj = (Byte)obj;
			if(btobj.intValue() == 1) {
				bool = true;
			}
		}else if(obj instanceof String){
			String strobj = (String)obj;
			bool = Boolean.parseBoolean(strobj);
		}
		return bool;
	}
	
	/**
	 * 转换为double
	 */
	public static double parseDouble(Object obj){
		double db = 0;
		if(obj instanceof BigDecimal){
			BigDecimal bdobj = (BigDecimal)obj;
			db = bdobj.doubleValue();
		}else if(obj instanceof String){
			String strobj = (String)obj;
			db = Double.parseDouble(strobj);
		}
		return db;
	}
	
	/**
	 * 转换为Integer
	 */
	public static int parseInteger(Object obj){
		int i = 0;
		if(obj instanceof BigDecimal){
			BigDecimal bdobj = (BigDecimal)obj;
			i = bdobj.intValue();
		}else if(obj instanceof String){
			String strobj = (String)obj;
			i = Integer.parseInt(strobj);
		}
		return i;
	}

}
