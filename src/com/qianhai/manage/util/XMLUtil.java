/**
 * 
 */
package com.qianhai.manage.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

/**
 * @author Administrator
 * 
 */
public class XMLUtil {

	@SuppressWarnings("unchecked")
	public static List<Element> parseXMLByFileName(String fileName) {
		List<Element> list = new ArrayList<Element>();
		File inputXML = new File(fileName);
		if(inputXML.exists()) {
			SAXReader saxReader = new SAXReader();
			try {
				Document document = saxReader.read(inputXML);
				Element root = document.getRootElement();
	            Iterator<Element> allSons = root.elementIterator();
	            while (allSons.hasNext()) {
	                getLeafNodes(list, allSons.next());
	            }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public static List<Element> parseXMLByURL(URL url) {
		List<Element> list = new ArrayList<Element>();
		try {
			InputStream in = url.openStream();
			SAXReader saxReader = new SAXReader();
			Document document = saxReader.read(in);
			Element root = document.getRootElement();
            Iterator<Element> allSons = root.elementIterator();
            while (allSons.hasNext()) {
                getLeafNodes(list, allSons.next());
            }
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public static List<Element> parseXMLByPath(String location) {
		List<Element> list = new ArrayList<Element>();
		ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		try {
			Resource[] resources = resolver.getResources(location);
			for(Resource resource : resources) {
				List<Element> tmp = parseXMLByURL(resource.getURL());
				list.addAll(tmp);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public static List<Element> selectNodes(String location, String nodename) {
		List<Element> list = new ArrayList<Element>();
		ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		try {
			Resource[] resources = resolver.getResources(location);
			for(Resource resource : resources) {
				InputStream in = resource.getInputStream();
				SAXReader saxReader = new SAXReader();
				Document document = saxReader.read(in);
				List<Element> tmp = document.selectNodes(nodename);
				list.addAll(tmp);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	private static void getLeafNodes(List<Element> allLeafs, Element currentNode) {
        Element e = currentNode;
        if ((e.elements()).size() >= 0)
        {
            List<Element> el = e.elements();
            for (Element sonNode : el)
            {
                if (sonNode.elements().size() > 0)
                    getLeafNodes(allLeafs, sonNode);
                else
                    allLeafs.add(sonNode);
            }
        }
    }

}
