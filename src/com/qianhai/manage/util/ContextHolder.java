/**
 * 
 */
package com.qianhai.manage.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Administrator
 *
 */
public class ContextHolder {
	
	private Map<String, Object> contexts;
	
	public ContextHolder() {
		contexts = new HashMap<String, Object>();
	}
	
	public Object get(String key) {
		return contexts.get(key);
	}
	
	public void put(String key, Object value) {
		contexts.put(key, value);
	}
	
	public void clear() {
		contexts.clear();
	}
	
	public Object remove(String key) {
		return contexts.remove(key);
	}

}
