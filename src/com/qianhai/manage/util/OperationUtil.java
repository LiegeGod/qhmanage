/**
 * 
 */
package com.qianhai.manage.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.security.crypto.codec.Base64;

/**
 * @author Administrator
 * 
 */
public class OperationUtil {

	public final static int ONE = 1;
	public final static int TWO = 1;

	public static String encode(String value) {
		byte[] bytes = value.getBytes();
		bytes = operation4code(bytes, ONE);
		byte[] _bytes = Base64.encode(bytes);
		_bytes = operation4code(_bytes, TWO);
		return new String(_bytes);
	}

	public static String decode(String value) {
		byte[] bytes = value.getBytes();
		bytes = operation4code(bytes, TWO);
		byte[] _bytes = Base64.decode(bytes);
		_bytes = operation4code(_bytes, ONE);
		return new String(_bytes);
	}

	private static byte[] operation4code(byte[] bytes, int type) {
		int length = bytes.length;
		byte[] _bytes = bytes;
		if (ONE == type) {
			byte tmp = _bytes[0];
			_bytes[0] = _bytes[length - 1];
			_bytes[bytes.length - 1] = tmp;
		} else if (TWO == type) {
			int code_index = (length / 2) + 1;
			if (code_index > length) {
				code_index = (length / 2);
			}
			byte last_code = _bytes[length - 1];
			byte index_code = _bytes[code_index];
			_bytes[length - 1] = index_code;
			_bytes[code_index] = last_code;

			byte[] new_encode = new byte[length];
			for (int i = 0; i < length; i++) {
				new_encode[i] = _bytes[length - 1 - i];
			}
			_bytes = new_encode;
			last_code = _bytes[length - 1];
			_bytes[length - 1] = _bytes[0];
			_bytes[0] = last_code;
		}
		return _bytes;
	}

	public static String MD5(String value) {
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			byte[] srcBytes = value.getBytes();
			byte[] resultBytes = md5.digest(srcBytes);
			int i;
			StringBuffer buf = new StringBuffer();
			for (int offset = 0; offset < resultBytes.length; offset++) {
				i = resultBytes[offset];
				if (i < 0) {
					i += 256;
				}
				if (i < 16) {
					buf.append("0");
				}
				buf.append(Integer.toHexString(i));
			}
			return buf.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
//	public static void main(String[] args) {
//		String value = "1qaz@WSX";
//		String vv = MD5(value);
//		System.out.println(vv);
//	}
	
//	public static void main(String[] args) {
//		File file = new File("F:\\MyApp\\tk.eclipse.plugin.htmleditor_2.2.0.jar");
//		Long time = file.lastModified();
//		System.out.println(time);
//		Date date = new Date(time);
//		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		System.out.println(df.format(date));
//	}
	
//	public static void main(String[] args) {
//		String str = "/root/show-web/lib/webapp/static/wx_static/site_media/html/a67da044a7d44dc994b9d089c9e8de7d.html";
//		String[] path = str.split("site_media/");
//		System.out.println(path[0]);
//		System.out.println(path[1]);
//	}

}
