/**
 * 
 */
package com.qianhai.manage.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.context.ApplicationContext;

/**
 * @author Administrator
 *
 */
public class ContextUtil {
	
	private static final Log log = LogFactory.getLog(ContextUtil.class);
	
	private static final String TAG = "ContextUtil";

	/**
	 * HttpSession范围。
	 */
	public static final String SCOPE_SESSION = "base_sessionContextHolder";
	/**
	 * 请求范围。
	 */
	public static final String SCOPE_REQUEST = "base_requestContextHolder";
	/**
	 * DI容器范围。
	 */
	public static final String SCOPE_APPLICATION = "base_applicationContextHolder";

	/**
	 * 缓存的DI容器。
	 */
	private static ApplicationContext applicationContext;
	/**
	 * 应用部署位置。
	 */
	private static String applicationPath;

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public static void setApplicationContext(
			ApplicationContext applicationContext) {
		ContextUtil.applicationContext = applicationContext;
	}

	public static String getApplicationPath() {
		return applicationPath;
	}

	public static void setApplicationPath(String applicationPath) {
		ContextUtil.applicationPath = applicationPath;
	}
	
	/**
	 * 查找Spring受管Bean。
	 * @param beanId String
	 * @return Object
	 */
	public static Object getBean(String beanId) {
		if(beanId == null || "".equals(beanId) || "null".equalsIgnoreCase(beanId)) {
			return null;
		}  else {
			return getBeanByBeanIdOrClass(beanId, null);
		}
	}
	
	/**
	 * 查找Spring受管Bean。
	 * @param clz Class
	 * @return List
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List getBeansByClass(Class clz) {
		List result = new ArrayList();
		if(applicationContext == null || clz == null) {
			return result;
		}
		Map beans = BeanFactoryUtils.beansOfTypeIncludingAncestors(applicationContext, clz, true, true);
		if(beans.isEmpty()) {
			log.info((new StringBuffer()).append("No bean of type").append(clz.toString()).append(" found.").toString());
		}
		return new ArrayList(beans.values());
	} 
	
	/**
	 * 查找Spring受管Bean。
	 * @param beanId String
	 * @param clz Class
	 * @return Object
	 */
	@SuppressWarnings("rawtypes")
	public static Object getBeanByBeanIdOrClass(String beanId,  Class clz) {
		if(applicationContext == null || "null".equalsIgnoreCase(beanId)) {
			return null;
		}
		if(null != beanId && applicationContext.containsBean(beanId)) {
			return applicationContext.getBean(beanId);
		}
		List l = getBeansByClass(clz);
		if(l.isEmpty()) {
			return null;
		} else {
			return l.get(0);
		}
	}
	
	/**
	 * 查询消息资源。
	 * @param property String
	 * @return String
	 */
	public static String getMessage(String property) {
		ApplicationContext applicationContext = ContextUtil.getApplicationContext();
		try {
			String message = applicationContext.getMessage(property, null, null);
			return message;
		} catch(Exception e) {
			log.error(TAG, e);
			return null;
		}
	}
	
	/**
	 * 往ContextHolder存储内容。
	 * @param key String
	 * @param value Object
	 * @param scopeBeanId String
	 */
	public static void put(String key, Object value, String scopeBeanId) {
		ContextHolder contextHolder = (ContextHolder) getBean(scopeBeanId);
		if(null != contextHolder) {
			contextHolder.put(key, value);
		}
	}
	
	/**
	 * 从ContextHolder存储内容。
	 * @param key String
	 * @param scopeBeanId String
	 * @return Object
	 */
	public static Object get(String key, String scopeBeanId) {
		ContextHolder contextHolder = (ContextHolder) getBean(scopeBeanId);
		if(null != contextHolder) {
			return contextHolder.get(key);
		}
		return null;
	}
	
	/**
	 * 销毁ContextHolder中的所有内容。
	 * @param scopeBeanId String
	 */
	public static void clear(String scopeBeanId) {
		ContextHolder contextHolder = (ContextHolder) getBean(scopeBeanId);
		if(null != contextHolder) {
			contextHolder.clear();
		}
	}
	
	/**
	 * 删除ContextHolder中的key对应的内容。
	 * @param key String
	 * @param scopeBeanId String
	 * @return Object
	 */
	public static Object remove(String key, String scopeBeanId) {
		ContextHolder contextHolder = (ContextHolder) getBean(scopeBeanId);
		if(null != contextHolder) {
			return contextHolder.remove(key);
		}
		return null;
	}
	
}
