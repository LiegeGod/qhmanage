/**
 * 
 */
package com.qianhai.manage.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Administrator
 *
 */
public class DateUtil {
	
	public static List<Date> getWeekend() {
		Calendar calendar = Calendar.getInstance(); //当前日期
        int currentyear = calendar.get(Calendar.YEAR);
        int nextyear = 1 + calendar.get(Calendar.YEAR);
        Calendar cstart = Calendar.getInstance();
        Calendar cend = Calendar.getInstance();
        
        cstart.set(currentyear, 0, 1);
        cend.set(nextyear, 0, 1);
        
        calendar.add(Calendar.DAY_OF_MONTH,-calendar.get(Calendar.DAY_OF_WEEK)); //周六
        
        Calendar d = (Calendar)calendar.clone();
        
        List<Date> data = new ArrayList<Date>();
        
        //向前
        for(;calendar.before(cend)&&calendar.after(cstart);calendar.add(Calendar.DAY_OF_YEAR, -7)) {
        	data.add(calendar.getTime());
        }
        
        //向后
        for(;d.before(cend)&&d.after(cstart);d.add(Calendar.DAY_OF_YEAR, 7)) {
        	data.add(d.getTime());
        }
        
        Collections.sort(data);
        return data;
	}
	
	public static List<Date> getWeekend(int year) {
		Calendar calendar = Calendar.getInstance(); //当前日期
		calendar.set(Calendar.YEAR, year);
		int currentyear = year;
		int nextyear = 1 + year;
		Calendar cstart = Calendar.getInstance();
		Calendar cend = Calendar.getInstance();
		
		cstart.set(currentyear, 0, 1);
		cend.set(nextyear, 0, 1);
		
		calendar.add(Calendar.DAY_OF_MONTH,-calendar.get(Calendar.DAY_OF_WEEK)); //周六
		
		Calendar d = (Calendar)calendar.clone();
		
		List<Date> data = new ArrayList<Date>();
		
		//向前
		for(;calendar.before(cend)&&calendar.after(cstart);calendar.add(Calendar.DAY_OF_YEAR, -7)) {
			data.add(calendar.getTime());
		}
		
		//向后
		for(;d.before(cend)&&d.after(cstart);d.add(Calendar.DAY_OF_YEAR, 7)) {
			data.add(d.getTime());
		}
		
		Collections.sort(data);
		return data;
	}
	
	public static List<Date> getDay(int year) {
		Calendar calendar = Calendar.getInstance(); //当前日期
		calendar.set(Calendar.YEAR, year);
		int currentyear = year;
		int nextyear = 1 + year;
		Calendar cstart = Calendar.getInstance();
		Calendar cend = Calendar.getInstance();
		
		cstart.set(currentyear, 0, 1);
		cend.set(nextyear, 0, 1);
		
		Calendar d = (Calendar)calendar.clone();
		
		Set<Date> data = new HashSet<Date>();
		
		//向前
		for(;calendar.before(cend)&&calendar.after(cstart);calendar.add(Calendar.DAY_OF_YEAR, -1)) {
			data.add(calendar.getTime());
		}
		
		//向后
		for(;d.before(cend)&&d.after(cstart);d.add(Calendar.DAY_OF_YEAR, 1)) {
			data.add(d.getTime());
		}
		
		List<Date> ndata = new ArrayList<Date>();
		ndata.addAll(data);
		
		Collections.sort(ndata);
		return ndata;
	}

}
