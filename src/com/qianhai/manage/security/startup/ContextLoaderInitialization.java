/**
 * 
 */
package com.qianhai.manage.security.startup;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.qianhai.manage.util.ContextUtil;

/**
 * @author Administrator
 *
 */
public class ContextLoaderInitialization extends ContextLoaderListener {
	
	private static final Log logger = LogFactory.getLog(ContextLoaderInitialization.class);
	
	private static ApplicationContext context;
	
	private static ServletContext servletContext;

	public static ApplicationContext getContext() {
		return context;
	}

	public static void setContext(ApplicationContext context) {
		ContextLoaderInitialization.context = context;
	}

	public static ServletContext getServletContext() {
		return servletContext;
	}

	public static void setServletContext(ServletContext servletContext) {
		ContextLoaderInitialization.servletContext = servletContext;
	}
	
	/**
	 * 初始化DI容器。
	 * @param event ServletContextEvent
	 */
	public void contextInitialized(ServletContextEvent event) {
		final ServletContext servletContext = event.getServletContext();
		setServletContext(servletContext);
		super.contextInitialized(event);
		
		setContext(WebApplicationContextUtils.getRequiredWebApplicationContext(event.getServletContext()));
		ContextUtil.setApplicationContext(ContextLoaderInitialization.context);
		ContextUtil.setApplicationPath(event.getServletContext().getRealPath("/"));
	}
	
	/**
	 * 销毁DI容器。
	 * @param event ServletContextEvent
	 */
	public void contextDestroyed(ServletContextEvent event) {
		logger.info("销毁DI容器。");
		super.contextDestroyed(event);
	}

}
