/**
 * 
 */
package com.qianhai.manage.security.filter;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.dom4j.Element;

import com.qianhai.manage.util.OperationUtil;
import com.qianhai.manage.util.StringUtil;
import com.qianhai.manage.util.XMLUtil;

/**
 * @author Administrator
 *
 */
public class LoginFilter implements Filter {
	
	private String filterLocation;
	private Set<String> excludes;
	
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
			FilterChain filterChain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		HttpSession session = request.getSession();
		ServletContext ServletContext = session.getServletContext();
		
		//判断过滤
		String currentURL = request.getRequestURI();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
		System.out.println("[TRACE] " + sdf.format(new Date()) + " \t当前访问路径：" + currentURL);
		String loginpage = StringUtil.addDefault((String)ServletContext.getAttribute("loginpage"), "login.jsp");
		this.excludes.add(loginpage);
		for(String exclude : excludes){
			if(currentURL.indexOf(exclude) != -1){
				filterChain.doFilter(request, response);
				return;
			}
		}
		
		String username = (String) session.getAttribute("username");
		if(!StringUtil.isEmpty(username)) {
			String validate = (String) session.getAttribute("user_validate");
			String value = OperationUtil.decode(validate);
			String _value = username + "[&%&]" + "qhym";
			if(_value.equals(value)) {
				filterChain.doFilter(request, response);
			} else {
				response.sendRedirect(request.getContextPath() +"/"+ loginpage);
			}
		} else {
			//跳转登录页
			response.sendRedirect(request.getContextPath() +"/"+ loginpage);
		}
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig config) throws ServletException {
		this.excludes = new TreeSet<String>();
		this.filterLocation = config.getInitParameter("filterLocation");
		if(!StringUtil.isEmpty(filterLocation)) {
			String filterExcludePaths_tag = "/filter/exclude/path";
			List<Element> filterExcludePaths = XMLUtil.selectNodes(filterLocation, filterExcludePaths_tag);
			for(Element element : filterExcludePaths) {
				String path = element.getTextTrim();
				if(!StringUtil.isEmpty(path)) {
					this.excludes.add(path);
				}
			}
		}
	}

}
