<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>首页</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link type="text/css" href="${ctx }/resources/css/style.css" rel="stylesheet" />
	<!-- Le styles -->
	<link type="text/css" href="${ctx }/resources/css/jquery-ui-1.9.2.custom.css" rel="stylesheet" />

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<link rel="stylesheet" href="${ctx }/resources/themes/gray/easyui.css" type="text/css"></link>
	<link rel="stylesheet" href="${ctx }/resources/themes/icon.css" type="text/css"></link>
	<link rel="stylesheet" href="${ctx }/resources/themes/color.css" type="text/css"></link>
	<script src="${ctx }/resources/js/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="${ctx }/resources/js/easyui/jquery.easyui.min.js" type="text/javascript"></script>
	<script src="${ctx }/resources/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
	<script src="${ctx }/resources/js/default.js" type="text/javascript"></script>
  </head>
  
  <body>
  	<div class="header"></div>
  	<div class="main">
  		<table border="0" cellspacing="0" width="100%">
  			<tr>
  				<td class="left" valign="top">
  					<div class="index-menu">
  						<ul id="menu">
							<li><a href="javascript:void(0);" onclick="addTab('1', '文章类别管理' , '${ctx }/news/newstype.do')">文章类别管理</a></li>
							<li><a href="javascript:void(0);" onclick="addTab('2', '文章管理' , '${ctx }/news/newsinit.do')">文章管理</a></li>
							<li>
							<a href="javascript:void(0);">前台管理</a>
							<ul>
								<li><a href="javascript:void(0);" onclick="addTab('3', 'Banner管理' , '${ctx }/news/banner.do')">Banner管理</a></li>
								<li><a href="javascript:void(0);" onclick="addTab('4', 'News管理' , '${ctx }/news/banner4news.do')">News管理</a></li>
								<li><a href="javascript:void(0);" onclick="addTab('5', '比赛管理' , '${ctx }/competition/competitionmanage.do')">比赛管理</a></li>
								<li><a href="javascript:void(0);" onclick="addTab('6', '比赛派奖管理' , '${ctx }/competition/awardprizes.do')">比赛派奖管理</a></li>
								<li><a href="javascript:void(0);" onclick="addTab('8', '商品管理' , '${ctx }/wares/manage.do')">商品管理</a></li>
							</ul>
							</li>
							<li><a href="javascript:void(0);" onclick="addTab('7', '交易工作日管理' , '${ctx }/symbol/workdayinit.do')">交易工作日管理</a></li>
							<li>
							<a href="javascript:void(0);">系统管理</a>
							<ul>
								<li><a href="javascript:void(0);" onclick="alert('正在开发中……')">用户类别管理</a></li>
								<li><a href="javascript:void(0);" onclick="alert('正在开发中……')">用户管理</a></li>
							</ul>
							
							</li>
						</ul>
  					</div>
  				</td>
  				<td class="right" valign="top">
  					<div id="slidetabs">
						<ul>
							<li><a href="#welcome">欢迎</a></li>
						</ul>
						<div id="welcome" class="main-view"><p>欢迎！</p></div>
					</div>
  				</td>
  			</tr>
  		</table>
  	</div>
  	<div class="logout">
  		<a class="logout-btn" href="javascript:void(0);">退出</a>
  	</div>
  
	<script type="text/javascript">
		$(function(){
			$('#slidetabs').tabs();
			$('#menu').menu();
			$(".logout-btn").on("click", function(){
				window.location.href = "${ctx }/public/logout.do";
			});
		});
		
		function closeTab(id) {
			var tabs = $('#slidetabs').tabs();
			tabs.find( ".ui-tabs-nav" ).find("li[aria-controls="+id+"]").children("span.ui-icon-close").click();
		}
		
		function addTab(id, label, url, callback) {
			var tabs = $('#slidetabs').tabs();
			var tabTemplate = "<li><a href='{href}'>{label}</a> <span class='ui-icon ui-icon-close'>Remove Tab</span></li>";
			
			var li = $( tabTemplate.replace( /\{href\}/g, "#" + id ).replace( /\{label\}/g, label ) ),
				content = "<iframe src='" + url + "' frameborder='0' style='border:0;width:100%;height:100%;'></iframe>";
				
			var length = tabs.find("#" + id).length;
			
			if(length == 0) {
				tabs.find( ".ui-tabs-nav" ).append( li );
				var $div = $("<div id='" + id + "' class='main-view'>" + content + "</div>");
				$div.data("callback", callback);
				tabs.append( $div );
				tabs.tabs( "refresh" );
			}
			
			tabs.find( ".ui-tabs-nav" ).find("li[aria-controls="+id+"]").children("a").click();
			
			tabs.on( "click", "span.ui-icon-close", function() {
				var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
				var action = $( "#" + panelId ).data("callback");
				$( "#" + panelId ).remove();
				if(action) {
					action();
				}
				tabs.tabs( "refresh" );
			});
		}
	</script>
  </body>
</html>
