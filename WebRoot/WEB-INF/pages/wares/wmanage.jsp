<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<script type="text/javascript">
<!--
	function showwin(id, title, src){
		parent.addTab(id, title, src, function() {
			$('#waresgrid').datagrid("reload");
		});
	}
	
	function showmywin(id, title, src, width, height, flag) {
		$("#mywin").window({ 
			title:title, 
		    minimizable:false,
		    collapsible:false, 
		    modal:true,
		    width:width ? width : document.body.clientWidth,
		    height:height ? height : document.body.clientHeight,
		    content : "<iframe scrolling='auto' frameborder='0' src='"+src+"' style='width:100%; height:99.5%'></iframe>",
		    onClose:function(){ 
		    	if(flag) {
			        $('#waresgrid').datagrid('reload'); 
			        $('#usersgrid').datagrid('reload');
		    	}
	    	}
		});
	}
	
	function query() {
		$('#waresgrid').datagrid("reload");
	}
	
	function query2() {
		$('#usersgrid').datagrid("reload");
	}
	
	function addwares() {
		showwin("addWares", "添加商品", "${ctx }/wares/editWares.do");
	}
	
	function editwares() {
		var selectedrow = $("#waresgrid").datagrid("getSelected");
		if(!selectedrow) {
			$.messager.alert("信息", "请<font color=\"red\">选择</font>一条记录", "info");
			return;
		}
		showwin("editWares", "修改商品", "${ctx }/wares/editWares.do?id=" + selectedrow.id);
	}
	
	function ClickRow(rowIndex, rowData) {
		var id = rowData.id;
		$('#usersgrid').datagrid("load", {wid:id});
	}
	
	function deletewares() {
		var selectedrows = $("#waresgrid").datagrid("getChecked");
		if(selectedrows.length == 0){
			$.messager.alert("信息", "请至少<font color=\"red\">勾选</font>一条记录", "info");
			return;
		}
		var ids = "";
		for(var i=0; i<selectedrows.length; i++) {
			ids += selectedrows[i].id + ",";
		}
		if(ids != "" && ids.length > 0) {
			ids = ids.substring(0, ids.length - 1);
		}
		$.ajax({
			url: "${ctx}/wares/deleteWares.do",
			type: "POST",
			data: {ids:ids},
			async: false,
			success: function(data) {
				if(data && data.msg) {
					if(data.msg == "OK") {
						$.messager.alert("信息", "成功删除！", "info");
						query();
					} else {
						$.messager.alert("信息", "删除失败！", "info");
					}
				}
			}
		});
	}
	
	function seeusers() {
		var selectedrow = $("#usersgrid").datagrid("getSelected");
		if(!selectedrow) {
			$.messager.alert("信息", "请<font color=\"red\">选择</font>一条记录", "info");
			return;
		}
		showmywin("seeusers", "查看人员详细信息", "${ctx }/wares/seewares2users.do?uid=" + selectedrow.uid, 400, 400, false);
	}
//-->
</script>
<div id="mywin"></div>
<div style="width:1200px; height: 500px; border: 0; position: static;">
<div id="tb">
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="addwares()">添加商品</a>
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="editwares()">修改商品</a>
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="deletewares()">删除商品</a>
</div>
<div id="waresgriddiv" style="position: relative;top: 0;width:100%;height:60%;">
	<table class="easyui-datagrid" id="waresgrid" data-options="url:'queryWares.do', pagination:true, pageSize:10,
	remoteSort:true, toolbar:'#tb',onClickRow:ClickRow, fit:true, nowrap:false, checkOnSelect:false, selectOnCheck:false, singleSelect:true">
		<thead>
			<tr>
				<th data-options="field:'ck',checkbox:true"></th>
				<th data-options="field:'name', sortable:true" width="250">商品名称</th>
				<th data-options="field:'beforeName', sortable:true" width="250">开卖前名称</th>
				<th data-options="field:'waresType', sortable:true, 
				formatter : function(value, rowData, rowIndex) {
				if(value=='1'){
				return '抢购';
				} else {
				return '普通';
				}
				}" width="250">商品类别</th>
				<th data-options="field:'waresKind', sortable:true, 
				formatter : function(value, rowData, rowIndex) {
				if(value=='1'){
				return '现金';
				} else {
				return '非现金';
				}
				}" width="250">商品种类</th>
				<th data-options="field:'goldType', sortable:true, 
				formatter : function(value, rowData, rowIndex) {
				if(value=='1'){
				return '金币';
				} else {
				return '现金'
				}
				}" width="450">商品货币类别</th>
				<th data-options="field:'gold', sortable:true" width="200">商品价格</th>
				<th data-options="field:'money', sortable:true" width="200">现金金额</th>
				<th data-options="field:'sellingTime', sortable:true" width="200">开卖/抢开始时间</th>
				<th data-options="field:'duration', sortable:true, 
				formatter : function(value, rowData, rowIndex) {
				var unit = rowData.durationUnit;
				var u_name = '天';
				if(unit == '1'){
				u_name = '秒'
				} else if(unit == '2'){
				u_name = '分'
				} else if(unit == '3'){
				u_name = '时'
				} else if(unit == '4'){
				u_name = '天'
				} else if(unit == '5'){
				u_name = '月'
				} else if(unit == '6'){
				u_name = '年'
				}
				return value + u_name;
				}" width="200">持续时间</th>
				<th data-options="field:'num', sortable:true" width="200">数量</th>
				<th data-options="field:'description', sortable:true" width="200">描述</th>
				<th data-options="field:'beforeDescription', sortable:true" width="200">开卖前描述</th>
			</tr>
		</thead>
	</table>
</div>
<div id="tb2" style="text-align: center">
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="seeusers()">显示详细信息</a>
</div>
<div id="wares2usersgriddiv" style="position: relative;top: 0;width:100%;height:40%;">
<table class="easyui-datagrid" id="usersgrid" data-options="url:'queryWares2Users.do', pagination:true, pageSize:10,
remoteSort:true, toolbar:'#tb2', fit:true, nowrap:false, checkOnSelect:false, selectOnCheck:false, singleSelect:true">
	<thead>
		<tr>
			<th data-options="field:'wname', sortable:true" width="250">商品名称</th>
			<th data-options="field:'uname', sortable:true" width="250">用户名</th>
			<th data-options="field:'time', sortable:true,
			formatter : function(value, rowData, rowIndex) {
			if (value == undefined) {
            return '';
        	}
        	var unixTimestamp = new Date(value);  
            return unixTimestamp.toLocaleString();
			}" width="250">购买时间</th>
			<th data-options="field:'num', sortable:true" width="250">购买数量</th>
			<th data-options="field:'flag', sortable:true,
			formatter : function(value, rowData, rowIndex) {
			if(value == '1') {
			return '已购买';
			} else {
			return '抢不到';
			}
			}" width="200">商品购买状态</th>
		</tr>
	</thead>
</table>

</div>
</div>