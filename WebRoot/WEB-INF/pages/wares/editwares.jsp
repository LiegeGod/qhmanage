<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>

<!-- include libries(jQuery, bootstrap, fontawesome) -->
<link href="${ctx }/resources/css/news.css" rel="stylesheet"> 
<link href="${ctx }/resources/css/datepicker.min.css" rel="stylesheet"> 
<link href="${ctx }/resources/css/bootstrap.css" rel="stylesheet"> 
<script src="${ctx }/resources/js/bootstrap.js"></script> 
<link href="${ctx }/resources/css/font-awesome.css" rel="stylesheet">
<link type="text/css" href="${ctx }/resources/css/easydialog.css" rel="stylesheet" />
 
<!-- include summernote css/js-->
<link href="${ctx }/resources/css/summernote.css" rel="stylesheet">
<link href="${ctx }/resources/css/wares.css" rel="stylesheet">
<script src="${ctx }/resources/js/summernote.min.js"></script>
 
<!--国际化 -->
<script src="${ctx }/resources/js/lang/summernote-zh-CN.js"></script>
<script src="${ctx }/resources/js/easydialog.min.js" type="text/javascript"></script>
<script src="${ctx }/resources/js/default.js" type="text/javascript"></script>
<script src="${ctx }/resources/js/json2.js" type="text/javascript"></script>
<script src="${ctx }/resources/js/wares.js" type="text/javascript"></script>
<script src="${ctx }/resources/js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
<script src="${ctx }/resources/js/lang/jquery.ui.datepicker-zh-cn.js"></script>
<script src="${ctx }/resources/js/jquery-ui-timepicker-addon.js"></script>
<style>
<!--

-->
</style>
<div>
<s:form class="form-news form-horizontal" id="myForm" modelAttribute="vo">
<table border="0" id="warestable" class="wares-table">
	<tbody>
		<tr>
			<td class="tname"><label>商品名称：</label></td>
			<td class="tvalue">
			<s:input path="name"/>
			<s:hidden path="id"/>
			</td>
			<td class="tname"><label>开卖前名称：</label></td>
			<td class="tvalue">
			<s:input path="beforeName"/>
			</td>
		</tr>
		<tr>
			<td class="tname"><label>商品类别：</label></td>
			<td class="tvalue">
			<s:select path="waresType">
				<s:option value="1">抢购</s:option>
				<s:option value="2">普通</s:option>
			</s:select>
			</td>
			<td class="tname"><label>商品种类：</label></td>
			<td class="tvalue">
			<s:select path="waresKind">
				<s:option value="0">非现金</s:option>
				<s:option value="1">现金</s:option>
			</s:select>
			</td>
		</tr>
		<tr>
			<td class="tname"><label>商品货币类别称：</label></td>
			<td class="tvalue">
			<s:select path="goldType">
				<s:option value="1">金币</s:option>
				<s:option value="2">现金</s:option>
			</s:select>
			</td>
			<td class="tname"><label>商品价格：</label></td>
			<td class="tvalue">
			<s:input path="gold"/>
			</td>
		</tr>
		<tr>
			<td class="tname" colspan="2">
			<label>开卖/抢开始时间：</label><a class="btn btn-primary add-btn" href="javascript:void(0);">添加时间</a>
			</td>
			<td class="tname"><label class="smoney">现金金额：</label></td>
			<td class="tvalue">
			<s:input path="money"/>
			</td>
		</tr>
		<tr>
			<td class="tname-s" colspan="4">
			<s:hidden path="sellingTime"/>
			<div id="dateinput"></div>
			</td>
		</tr>
		<tr>
			<td class="tname"><label>持续时间（0代表1年有效）：</label></td>
			<td class="tvalue">
			<s:input path="duration"/>
			</td>
			<td class="tname"><label>持续时间单位：</label></td>
			<td class="tvalue">
			<s:select path="durationUnit" >
				<s:option value="4">天</s:option>
				<s:option value="1">秒</s:option>
				<s:option value="2">分</s:option>
				<s:option value="3">时</s:option>
				<s:option value="5">月</s:option>
				<s:option value="6">年</s:option>
			</s:select>
			</td>
		</tr>
		<tr>
			<td class="tname"><label>商品数量：</label></td>
			<td class="tvalue">
			<s:input path="num"/>
			</td>
			<td class="tname"><label>是否随机挑选：</label></td>
			<td class="tvalue">
			<s:select path="randomFlag">
				<s:option value="0">否</s:option>
				<s:option value="1">是</s:option>
			</s:select>
			</td>
		</tr>
		<tr>
			<td class="tname"><label>描述：</label></td>
			<td class="tvalue">
			<s:textarea path="description" rows="2" cols="17"/>
			</td>
			<td class="tname"><label>开卖前描述：</label></td>
			<td class="tvalue">
			<s:textarea path="beforeDescription" rows="2" cols="17"/>
			</td>
		</tr>
		<tr>
			<td align="center" class="tname-s" colspan="4">
			<input type="hidden" id="picture" name="picture" value="${picture }">
			<a class=btn_addPic href="javascript:void(0);"><span><em>+</em>添加图片</span>
			<input class="filePrew" title="支持jpg、jpeg、gif、png格式，文件小于5M" tabIndex="5" multiple="multiple" 
			accept="image/png,image/jpeg,image/gif" type="file" size="5" name="selectfile">
			</a>
			</td>
		</tr>
		<tr>
			<td align="center" class="tname-s" colspan="4">
			<label>以下是上传的图片：</label>
			<div class="show-image-view">
				
			</div>
			</td>
		</tr>
	</tbody>
</table>
<center>
<a class="btn btn-primary submit-btn" href="javascript:void(0);">提交</a>
<a class="btn btn-primary clear-btn" href="javascript:void(0);">重置</a>
</center>
</s:form>
</div>