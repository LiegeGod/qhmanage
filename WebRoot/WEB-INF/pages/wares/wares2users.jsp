<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<script type="text/javascript">
<!--
//-->
</script>
<style type="text/css">
<!--
	#usermsg {
		border: thin;
	}
	
	#usermsg tr {
		min-height: 30px;
		line-height: 30px;
	}
-->
</style>
<div style="width:380px;border: 0">
<table id="usermsg" width="100%" border="1" cellpadding="0" cellspacing="0">
	<tr height="50">
		<td width="20%">用户ID</td>
		<td width="30%">
		<c:choose>
		<c:when test="${not empty result.headimgurl }">
		<img src="${result.headimgurl }" alt="" height="50">
		</c:when>
		<c:otherwise></c:otherwise>
		</c:choose>
		${result.uid }
		</td>
		<td width="20%">全称</td>
		<td width="30%">${result.fullname }</td>
	</tr>
	<tr>
		<td>微信号</td>
		<td>${result.wxno }</td>
		<td>昵称</td>
		<td>${result.nickname }</td>
	</tr>
	<tr>
		<td>性别</td>
		<td>
		<c:choose>
		<c:when test="${result.sex == 1 }">男</c:when>
		<c:otherwise>女</c:otherwise>
		</c:choose>
		</td>
		<td>国籍</td>
		<td>${result.country }</td>
	</tr>
	<tr>
		<td>省份</td>
		<td>${result.province }</td>
		<td>所在城市</td>
		<td>${result.city }</td>
	</tr>
	<tr>
		<td>手机号码</td>
		<td>${result.mobile }</td>
		<td>openid</td>
		<td>${result.openid }</td>
	</tr>
	<tr height="70">
		<td>住址</td>
		<td colspan="3">${result.address }</td>
	</tr>
</table>
</div>