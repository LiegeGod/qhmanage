<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>

<!-- include libries(jQuery, bootstrap, fontawesome) -->
<link href="${ctx }/resources/css/news.css" rel="stylesheet"> 
<link href="${ctx }/resources/css/datepicker.min.css" rel="stylesheet"> 
<link href="${ctx }/resources/css/bootstrap.css" rel="stylesheet"> 
<script src="${ctx }/resources/js/bootstrap.js"></script> 
<link href="${ctx }/resources/css/font-awesome.css" rel="stylesheet">
<link type="text/css" href="${ctx }/resources/css/easydialog.css" rel="stylesheet" />
 
<!-- include summernote css/js-->
<link href="${ctx }/resources/css/summernote.css" rel="stylesheet">
<script src="${ctx }/resources/js/summernote.min.js"></script>
 
<!--国际化 -->
<script src="${ctx }/resources/js/lang/summernote-zh-CN.js"></script>
<script src="${ctx }/resources/js/datepicker.min.js"></script>
<script src="${ctx }/resources/js/lang/datepicker.zh-CN.js"></script>
<script src="${ctx }/resources/js/easydialog.min.js" type="text/javascript"></script>
<script src="${ctx }/resources/js/default.js" type="text/javascript"></script>
<script src="${ctx }/resources/js/json2.js" type="text/javascript"></script>
<script src="${ctx }/resources/js/competition.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
	$(function() {
		createSelect();
	});
	
	function createSelect() {
		var $type = $("#type");
		$.ajax({
			url: "${ctx}/news/querynewstypebyl.do",
			type: "POST",
			data: {"value":"${leveltid }"},
			async: false,
			success: function(data) {
				if(data && data.result == 1) {
					var _list = data.list;
					var str = "<select id=\"type\" name=\"type\" class=\"news-type-value form-control\"></select>";
					var $str = $(str);
					$str.append("<option value=\"\">请选择</option>");
					for(var i=0; i<_list.length; i++) {
						$str.append("<option value=\"" + _list[i].value + "\">" + _list[i].name + "</option>");
					}
					$type.after($str);
					$type.remove();
				}
			}
		});
	}
//-->
</script>
<style type="text/css">
h4 {
	margin: 0;
}

.form-news {
	width: 80%;
	margin: 0 auto;
}

.news-title {
	height: 70px;
}

.news-type {
	height: 200px;
	margin-bottom: 60px;
}

.news-title-label, .news-type-label {
	font-size: 18px;
	font-weight: bold;
}

.news-content {
	margin-top: 20px;
}

.gold_type {
	text-align: center;
}
</style>

<form class="form-news form-horizontal" id="myForm">
<div class="news-title">
<label class="news-title-label" for="title">题目</label>
<div>
	<input id="title" name="title" class="news-title-value form-control" value="${vo.title }">
</div>
</div>
<div class="news-type">
	<div>
		<label>规则：</label>
		<div>
		<input type="hidden" id="rule" name="rule" class="form-control" value='${vo.rule }'>
		<a href="javascript:void(0);" class="btn btn-primary rule-btn">设置规则</a>
		</div>
	</div>
	<div>
		<label>类别：</label>
		<div>
		<input type="hidden" id="levelt" name="levelt" class="form-control" value="3">
		<input id="type" name="type" class="form-control" value="${vo.type }">
		</div>
	</div>
	<div>
		<table border="0" width="100%">
			<tr>
				<td>
				<label>费用：</label>
				<div>
				<input id="cost" name="cost" class="number form-control" value="${vo.cost }">
				</div>
				</td>
			</tr>
		</table>
		
	</div>
	<div>
		<table border="0" width="100%">
			<tr>
				<td>
				<label>开始时间：</label>
				<div>
				<input id="startTime" name="startTime" class="form-control" 
				value='<fmt:formatDate value="${vo.startTime }"/>' datepicker>
				</div>
				</td>
				<td>
				<label>结束时间：</label>
				<div>
				<input id="endTime" name="endTime" class="form-control" 
				value='<fmt:formatDate value="${vo.endTime }"/>' datepicker>
				</div>
				</td>
			</tr>
		</table>
		
	</div>
</div>
<div class="news-content">
<label class="news-title-label">内容</label>
<div style="display: none;"><input type="hidden" id="cid" name="cid" value="${vo.cid }"></div>
<div style="display: none;"><input type="hidden" id="pid" name="pid" value="${pid }"></div>
<div style="display: none;"><input type="hidden" id="content" name="content"></div>
<div id="summernote"></div>
</div>
<center>
<a class="btn btn-primary submit-btn" href="javascript:void(0);">提交</a>
<a class="btn btn-primary clear-btn" href="javascript:void(0);">重置</a>
</center>
</form>
<script type="text/javascript">
	$(function() {
		var $vo = "${vo }";
		if($vo != "" && $vo != "{}") {
			$("#summernote").code('${vo.content }');
			createSelect();
			$("#type").val("${vo.type }");
		}
	});
</script>