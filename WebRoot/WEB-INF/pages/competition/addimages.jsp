<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<link type="text/css" href="${ctx }/resources/css/addimage.css" rel="stylesheet" />
<script src="${ctx }/resources/js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
	$(function() {
		var files = new Array();
		
		$(".submit-btn").button().click(function() {
			submit();
		});
		
		function submit() {
			var _data = new FormData();
			for(var i=0; i<files.length; i++) {
		    	_data.append("files", files[i], files[i].name);
			}
		    var pid = $("#pid").val();
		    _data.append("pid", pid);
			$.ajax({
				url: "saveimage.do",
				type: "POST",
				async: false,
				data: _data,
				cache: false,
		        contentType: false,
		        processData: false,
				success: function(data) {
					if(data && data.result) {
						if(data.result == 1) {
							var msg = "保存成功!";
							parent.$.messager.confirm('信息', msg, function(r) {
								parent.$('#addImage').window('close');
							});
						} else {
							parent.$.messager.alert('错误', data.result, 'error');
						}
					}
				}
			});
		}
		
		$(".filePrew").on("change", function(e) {
			var _files = e.target.files||e.dataTransfer.files;
			if(_files) {
				for(var i=0; i<_files.length; i++) {
					files.push(_files[i]);
				 	var reader = new FileReader();
				 	reader.onload = function() {
					 	$(".show-image-view").append("<div><img src='"+this.result+"'/></div>");
				 	};
					reader.readAsDataURL(_files[i]);
				}
			}
		});
	});
//-->
</script>
<div class="image-controller">
	<div class="add-image-btn">
		<table border="0" width="100%">
			<tr>
				<td align="center">
				<a class=btn_addPic href="javascript:void(0);"><span><em>+</em>添加图片</span>
				<input class="filePrew" title="支持jpg、jpeg、gif、png格式，文件小于5M" tabIndex="5" multiple="multiple" 
				accept="image/png,image/jpeg,image/gif" type="file" size="5" name="selectfile">
				</a>
				</td>
			</tr>
			<tr>
				<td align="center">
				<label>以下是上传的图片：</label>
				<div class="show-image-view">
	
				</div>
				</td>
			</tr>
			<tr>
				<td align="center">
				<input type="hidden" id="pid" name="pid" value="competition">
				<a class="submit-btn" href="javascript:void(0);">提交</a>
				</td>
			</tr>
		</table>
	</div>
</div>