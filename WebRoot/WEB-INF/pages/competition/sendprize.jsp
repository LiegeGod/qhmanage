<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<script type="text/javascript">
<!--
	function awardPrizes() {
		var selectedrows = $('#prizegrid').datagrid('getChecked');
		if(selectedrows.length == 0){
			$.messager.alert('信息', '请选择至少一条记录。', 'info');
			clearselection();
	      	return;
		}
		var arr = new Array();
		for(var i=0; i<selectedrows.length; i++) {
			var uid = selectedrows[i].uid;
			var cid = selectedrows[i].cid;
			var json = {};
			json["uid"] = uid;
			json["cid"] = cid;
			arr.push(JSON.stringify(json));
		}
		if(arr.length > 0) {
			$.ajax({
				url: "updatePrizeState.do",
				type: "POST",
				async: false,
				data: {json:arr.toString()},
				success: function(data) {
					if(data && data.result) {
						var msg = "";
						if(data.result == 1) {
							msg = "派奖更新成功！";
						} else {
							msg = "派奖更新失败！";
						}
						$.messager.alert('信息', msg, 'info');
						$('#prizegrid').datagrid('reload');
					}
				}
			});
		}
	}
	
//-->
</script>
<div style="width:1200px;border: 0">
<div id="tb">
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="awardPrizes()">派奖</a>
</div>
<div id="prizegriddiv" style="height:615px;">
	<table class="easyui-datagrid" id="prizegrid" data-options="url:'queryprize.do', pagination:true, pageSize:10,
	remoteSort:true, toolbar:'#tb', fit:true, nowrap:false, checkOnSelect:false, selectOnCheck:false, singleSelect:true">
		<thead>
			<tr>
				<th data-options="field:'ck',checkbox:true"></th>
				<th data-options="field:'username', sortable:true" width="300">用户名</th>
				<th data-options="field:'cname', sortable:true" width="450">比赛名</th>
				<th data-options="field:'type', sortable:true, align:'center', 
				formatter:function(value, data, index) {
				if(value=='1') {
					return '现金';
				} else {
					return '金币';
				}
				}" width="100">奖金类型</th>
				<th data-options="field:'gold', sortable:true, align:'right'" width="200">奖金数量</th>
				<th data-options="field:'state', align:'center', 
				formatter:function(value, data, index) {
				if(value == '0') {
					return '<font color=red>未派奖</font>';
				} else {
					return '<font color=green>已派奖</font>';
				}
				}" width="100">状态</th>
			</tr>
		</thead>
	</table>

</div>
</div>