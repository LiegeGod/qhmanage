<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<script type="text/javascript">
<!--
	function showwin(id, title, src){
		showWindow({ 
			title: title,
			id: id,
			src: src,
		    minimizable:false,
		    collapsible:false, 
		    modal:true,
		    width:document.body.clientWidth*(2/3),
		    height:document.body.clientHeight*(2/3),
		    onClose:function(){
		    	$('#bannergrid').datagrid('reload');
		    }
		});
	}
	
	function showImage() {
		showwin("showImage", "查看图片", "${ctx }/competition/showimage.do");
	}
	
	function addImage() {
		showwin("addImage", "添加图片", "${ctx }/competition/addimage.do");
	}
//-->
</script>
<div style="width:1200px;border: 0">
<div id="tb">
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="showImage()">查看图片</a>
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="addImage()">添加图片</a>
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="deleteImage()">删除图片</a>
</div>
<div id="bannergriddiv" style="height:615px;">
	<table class="easyui-datagrid" id="bannergrid" data-options="url:'queryimages.do', pagination:true, pageSize:10,
	remoteSort:true, toolbar:'#tb', fit:true, nowrap:false, checkOnSelect:false, selectOnCheck:false, singleSelect:true">
		<thead>
			<tr>
				<th data-options="field:'ck',checkbox:true"></th>
				<th data-options="field:'tname', sortable:true" width="250">图片名</th>
				<th data-options="field:'name', sortable:true" width="800">图片地址</th>
			</tr>
		</thead>
	</table>

</div>
</div>