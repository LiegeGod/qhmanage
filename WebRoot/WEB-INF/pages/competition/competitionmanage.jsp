<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<script type="text/javascript">
<!--
	function showwin(id, title, src){
		parent.addTab(id, title, src, function() {
			$('#competitiongrid').datagrid('reload');
		});
	}
	
	function showwin2(id, title, src){
		showWindow({ 
			title: title,
			id: id,
			src: src,
		    minimizable:false,
		    collapsible:false, 
		    modal:true,
		    width:document.body.clientWidth*(2/3),
		    height:document.body.clientHeight*(2/3),
		    onClose:function(){
		    	$('#competitiongrid').datagrid('reload');
		    }
		});
	}
	
	function addcompetition() {
		showwin("addCompetition", "添加比赛", "${ctx }/competition/editcompetition.do");
	}
	
	function editcompetition() {
		var selectedrows = $('#competitiongrid').datagrid('getChecked');
		if(selectedrows.length > 1){
			$.messager.alert('信息', '只能选择一条记录。', 'info');
			clearselection();
	      	return;
		}
		if(selectedrows.length == 1) {
			var selectedrow = selectedrows[0];
			showwin("editCompetition", "修改比赛", "${ctx }/competition/editcompetition.do?cid=" + selectedrow.cid);
		} else {
			$.messager.alert('信息', '请选择选择一条记录。', 'info');
			return;
		}
	}
	
	function deletecompetition() {
		var selectedrows = $('#competitiongrid').datagrid('getChecked');
		if(selectedrows.length < 1) {
			$.messager.alert('信息', '请选择选择一条记录。', 'info');
			return;
		}
		var cids = new Array();
		for(var i=0; i<selectedrows.length; i++) {
			cids.push(selectedrows[i].cid);
		}
		$.messager.confirm("信息", "确定删除该比赛?", function(r) {
			if(r) {
				$.ajax({
					url: "deletecompetition.do",
					type: "POST",
					async: false,
					data: {cids:cids.join(",")},
					success: function(data) {
						if(data && data.result) {
							$.messager.alert('信息', '删除成功！', 'info');
							$('#competitiongrid').datagrid('reload');
						}
					}
				});
			}
		});
	}
//-->
</script>
<div style="width:1200px;border: 0">
<div id="tb">
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="addcompetition()">添加比赛</a>
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="editcompetition()">修改比赛</a>
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="deletecompetition()">删除比赛</a>
</div>
<div id="competitiongriddiv" style="height:615px;">
	<table class="easyui-datagrid" id="competitiongrid" data-options="url:'querycompetition.do', pagination:true, pageSize:10,
	remoteSort:true, toolbar:'#tb', fit:true, nowrap:false, checkOnSelect:false, selectOnCheck:false, singleSelect:true">
		<thead>
			<tr>
				<th data-options="field:'ck',checkbox:true"></th>
				<th data-options="field:'title', sortable:true" width="250">比赛题目</th>
				<th data-options="field:'typename', sortable:true" width="250">比赛类别</th>
				<th data-options="field:'rule', sortable:true" width="450">比赛规则</th>
				<th data-options="field:'cost', sortable:true" width="200">比赛报名费</th>
				<th data-options="field:'startTime', sortable:true,
				formatter:function(value, rowData, rowIndex) {
					var date = new Date(value);
					return date.format('yyyy-MM-dd');
				}" width="300">比赛开始时间</th>
				<th data-options="field:'endTime', sortable:true,
				formatter:function(value, rowData, rowIndex) {
					var date = new Date(value);
					return date.format('yyyy-MM-dd');
				}" width="300">比赛结束时间</th>
				<th data-options="field:'marks'" width="300">备注</th>
			</tr>
		</thead>
	</table>

</div>
</div>