<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<link href="${ctx }/resources/css/fullcalendar.min.css" rel="stylesheet" />
<link href="${ctx }/resources/css/fancybox.css" rel="stylesheet" />
<link href="${ctx }/resources/css/fullcalendar.print.css" rel="stylesheet" media="print" />
<script type="text/javascript" src="${ctx }/resources/js/moment.min.js"></script>
<script type="text/javascript" src="${ctx }/resources/js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="${ctx }/resources/js/fullcalendar.min.js"></script>
<script type="text/javascript" src="${ctx }/resources/js/jquery.fancybox-1.3.1.pack.js"></script>
<script type="text/javascript" src="${ctx }/resources/js/lang-all.js"></script>
<script type="text/javascript">
function tclick(){
	var url = "${ctx}/symbol/workday.do";
	alert(url);
	$.post(url, {data:$(".test").html()}, function(data){
		alert(data);
	});
}

$(function(){
	var date = new Date();
	$("#calendar").fullCalendar({
		header: {
			left: "prev",
			center: "title today",
			right: "next"
		},
		defaultDate: date.format("yyyy-MM-dd"),
		lang: "zh-cn",
		buttonIcons: false,
		weekNumbers: false,
		editable: false,
		eventLimit: false,
		events: "getEventData.do",
		dayClick: function(date, allDay, jsEvent, view) {
			date = new Date(date);
			var selDate = date.format("yyyy-MM-dd");
			$.fancybox({
				"type": "ajax",
				"href": "selecttimeinit.do?action=1&date="+selDate
			});
    	},
    	eventClick: function(calEvent, jsEvent, view) {
			$.fancybox({
				"type": "ajax",
				"href": "selecttimeinit.do?action=2&id="+calEvent.id
			});
    	}
	});
});
</script>
<style>
<!--
#calendar {
	max-width: 700px;
	margin: 0 auto;
}
.fancy{width:450px; height:auto}
.fancy h3{height:30px; line-height:30px; border-bottom:1px solid #d3d3d3; font-size:14px}
.fancy form{padding:10px}
.fancy p{height:28px; line-height:28px; padding:4px; color:#999}
.input{height:20px; line-height:20px; padding:2px; border:1px solid #d3d3d3; width:100px}
.btn{-webkit-border-radius: 3px;-moz-border-radius:3px;padding:5px 12px; cursor:pointer}
.btn_ok{background: #360;border: 1px solid #390;color:#fff}
.btn_cancel{background:#f0f0f0;border: 1px solid #d3d3d3; color:#666 }
.btn_del{background:#f90;border: 1px solid #f80; color:#fff }
.sub_btn{height:32px; line-height:32px; padding-top:6px; border-top:1px solid #f0f0f0; text-align:right; position:relative}
.sub_btn .del{position:absolute; left:2px}
-->
</style>
<div id="calendar"></div>