<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<link type="text/css" href="${ctx }/resources/css/jquery-ui-1.10.0.custom.css" rel="stylesheet" />
<div class="fancy">
	<h3><c:if test="${action==1 }">新建</c:if><c:if test="${action==2 }">编辑</c:if>事件</h3>
    <form id="add_form" action="saveworkday.do" method="post">
    <input type="hidden" id="id" name="id" value="${vo.id }">
    <p>原因：<input type="text" class="input" name="title" id="title" style="width:320px" 
    value="${vo.title }" placeholder="记录原因..."></p>
    <p>开始时间：
    <input type="text" class="input datepicker" name="startdate" id="startdate" 
    value="${startdate }">
    </p>
    <p id="p_endtime" style="display:none">
	结束时间：
    <input type="text" class="input datepicker" name="enddate" id="enddate" 
    value="${enddate }">
    </p>
    <p>
    <label><input type="checkbox" value="1" id="isend" name="isend"> 结束时间</label>
    </p>
    <div class="sub_btn">
    <span class="del">
    <input type="button" class="btn btn_del" id="del_event" value="删除">
    </span>
    <input type="submit" class="btn btn_ok" value="确定"> 
    <input type="button" class="btn btn_cancel" value="取消" onClick="$.fancybox.close()">
    </div>
    </form>
</div>
<script type="text/javascript" src="${ctx }/resources/js/jquery.form.min.js"></script>
<script type="text/javascript">
$(function(){
	$(".datepicker").datepicker({
		dateFormat: "yy-mm-dd",//日期格式  
        yearSuffix: "年", //年的后缀  
        showMonthAfterYear:true,//是否把月放在年的后面  
        monthNames: ["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],  
        dayNames: ["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],  
        dayNamesShort: ["周日","周一","周二","周三","周四","周五","周六"],  
        dayNamesMin: ["日","一","二","三","四","五","六"]
		});
	
	$("#isend").click(function(){
		if($("#p_endtime").css("display")=="none"){
			$("#p_endtime").show();
		}else{
			$("#p_endtime").hide();
			$("#enddate").datepicker("setDate", "");
		}
		$.fancybox.resize();//调整高度自适应
	});
	
	//提交表单
	$("#add_form").ajaxForm({
		beforeSubmit: showRequest, //表单验证
        success: showResponse //成功返回
    });
	
	if("${action}" == "1") {
		$(".del").hide();
	}
	
	if("${enddate}" != "") {
		$("#isend").click();
	}
	
	$("#del_event").click(function(){
		if(confirm("您确定要删除吗？")){
			var id = $("#id").val();
			$.post("deleteworkday.do", {id:id}, function(data){
				if(data && data.msg==1){//删除成功
					$.fancybox.close();
					$('#calendar').fullCalendar("refetchEvents"); //重新获取所有事件数据
				}else{
					alert(data.msg);	
				}
			});
		}
	});
});

function showRequest(){
	var events = $("#title").val();
	if(events==""){
		alert("请输入原因！");
		$("#event").focus();
		return false;
	}
	var stime = $("#startdate").val();
	var etime = $("#enddate").val();
	if("" != etime && $("#p_endtime").css("display") != "none") {
		stime = stime.replace("-", "");
		etime = etime.replace("-", "");
		if(stime > etime) {
			alert("请输入正确的日期！");
			return false;
		}
	} else {
		$("#enddate").datepicker("setDate", stime);
	}
}

function showResponse(responseText, statusText, xhr, $form){
	if(statusText=="success"){	
		if(responseText.data=="OK"){
			$.fancybox.close();
			$("#calendar").fullCalendar("refetchEvents"); //重新获取所有事件数据
		}else{
			alert(responseText);
		}
	}else{
		alert(statusText);
	}
}
</script>