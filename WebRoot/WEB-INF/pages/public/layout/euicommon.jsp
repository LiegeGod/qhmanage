<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<!-- Le styles -->
<link type="text/css" href="${ctx }/resources/css/jquery-ui-1.10.0.custom.css" rel="stylesheet" />
<link rel="stylesheet" href="${ctx }/resources/themes/gray/easyui.css" type="text/css"></link>
<link rel="stylesheet" href="${ctx }/resources/themes/icon.css" type="text/css"></link>
<link rel="stylesheet" href="${ctx }/resources/themes/color.css" type="text/css"></link>
<script type="text/javascript" src="${ctx }/resources/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="${ctx }/resources/js/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${ctx }/resources/js/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${ctx }/resources/js/dateExtend.js"></script>
<script type="text/javascript" src="${ctx }/resources/js/default.js"></script>
<link rel="stylesheet" href="${ctx }/resources/css/default.css" type="text/css"></link>
<title><tiles:insertAttribute name="title"/></title>
</head>
<body>
<tiles:insertAttribute name="content"/>
</body>
</html>