<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<script type="text/javascript" src="${ctx }/resources/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="${ctx }/resources/js/dateExtend.js"></script>
<title><tiles:insertAttribute name="title"/></title>
</head>
<body>
<tiles:insertAttribute name="content"/>
</body>
</html>