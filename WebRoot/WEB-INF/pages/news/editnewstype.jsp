<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<script src="${ctx }/resources/js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
	$(function() {
		var old_value = "";
		$(".number").on("blur", function() {
			var _value = $(this).val();
			var temp = /^\d+?$/;
			if(temp.test(_value)) {
				old_value = $(this).val();
			} else {
				$(this).val(old_value);
			}
		});
		
		$(".number").on("focus", function() {
			old_value = $(this).val();
		});
		
		$(".submit-btn").button().click(function() {
			submit();
		});
		$(".clear-btn").button().click(function() {
			clear();
		});
		
		function clear() {
			if($("#cpid").length > 0) {
				$("#cpid").val("");
			}
			$("#value").val("");
			old_value = "";
			$("#name").val("");
		}
		
		function submit() {
			$.ajax({
				url: "savenewstype.do",
				type: "POST",
				async: false,
				data: $("#myform").serialize(),
				success: function(data) {
					if(data && data.result) {
						if(data.result < 3) {
							var msg = "成功";
							var _that = parent;
							parent.$.messager.confirm('信息', (data.result == 1 ? "录入" : "修改") + msg, function(r) {
								var id = "addType";
								if(data.result == 2) {
									id = "editType";
								}
								parent.$('#' + id).window('close');
							});
						} else {
							parent.$.messager.alert('错误', data.result, 'error');
						}
					}
				}
			});
		}
		
		if($("#cpid").length > 0) {
			$("#cpid").on("change", function() {
				$("#pid").val($(this).val());
			});
		}
	});
	
//-->
</script>
<style type="text/css">
<!--
#myform {
	margin: 0 auto;
	margin-top: 20px;
	width: 50%;
}

.form-tr {
	margin-bottom: 20px;
	line-height: 20px;
}

.center-btn {
	margin-top: 20px;
}
-->
</style>
<div>
	<form action="" id="myform">
		<table>
			<c:if test="${ml != null }">
			<tr class="form-tr">
				<td>上级类型:</td>
				<td>
				<select id="cpid" name="cpid" style="width: 150px;">
					<option value="">请选择</option>
					<c:forEach items="${ml }" var="m">
					<option value="${m.cid }">${m.name }</option>
					</c:forEach>
				</select>
				</td>
			</tr>
			</c:if>
			<tr class="form-tr">
				<td>类型值:</td>
				<td><input type="text" id="value" name="value" value="${vo.value }"></td>
			</tr>
			<tr class="form-tr">
				<td>类型名称:</td>
				<td><input type="text" id="name" name="name" value="${vo.name }"></td>
			</tr>
		</table>
		<input type="hidden" id="cid" name="cid" value="${vo.cid }">
		<input type="hidden" id="pid" name="pid" value="${vo.pid }">
	</form>
	<center class="center-btn">
	<a class="submit-btn" href="javascript:void(0);">提交</a>
	<a class="clear-btn" href="javascript:void(0);">重置</a>
	</center>
</div>