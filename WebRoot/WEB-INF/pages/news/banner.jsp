<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<script type="text/javascript">
<!--
	function showwin(id, title, src){
		showWindow({ 
			title: title,
			id: id,
			src: src,
		    minimizable:false,
		    collapsible:false, 
		    modal:true,
		    width:document.body.clientWidth*(2/3),
		    height:document.body.clientHeight*(2/3),
		    onClose:function(){
		    	$('#bannergrid').datagrid('reload');
		    }
		});
	}
	
	function showwin2(id, title, src){
		
		parent.addTab(id, title, src, function() {
			$('#bannergrid').datagrid('reload');
		});
	}
	
	function openwindow(id) {
		var url = "${ctx }/news/shownews.do?cid=" + id;
		window.open(url);
	}
	
	function addNews(){
		showwin2("addNews", "录入文章", "${ctx }/news/addnews.do");
	}
	
	function shownews() {
		var selectedrows = $('#bannergrid').datagrid('getChecked');
		if(selectedrows.length > 1){
			$.messager.alert('信息', '只能选择一条记录。', 'info');
			clearselection();
	      	return;
		}
		if(selectedrows.length == 1) {
			var selectedrow = selectedrows[0];
			openwindow(selectedrow.cid);
		} else {
			$.messager.alert('信息', '请选择选择一条记录。', 'info');
			return;
		}
	}
	
	function createbanner(){
		var selectedrows = $('#bannergrid').datagrid('getChecked');
		if(selectedrows.length > 1){
			$.messager.alert('信息', '只能选择一条记录。', 'info');
			clearselection();
	      	return;
		}
		if(selectedrows.length == 1) {
			var selectedrow = selectedrows[0];
			showwin("createBanner", "创建Banner", "${ctx }/news/editbanner.do?type=1&cid=" + selectedrow.cid);
		} else {
			$.messager.alert('信息', '请选择选择一条记录。', 'info');
			return;
		}
	}
	
	function deletenews() {
		var selectedrows = $('#bannergrid').datagrid('getChecked');
		if(selectedrows.length < 1) {
			$.messager.alert('信息', '请选择选择一条记录。', 'info');
			return;
		}
		var cids = new Array();
		for(var i=0; i<selectedrows.length; i++) {
			cids.push(selectedrows[i].cid);
		}
		$.ajax({
			url: "deletenews.do",
			type: "POST",
			async: false,
			data: {cids:cids.join(",")},
			success: function(data) {
				if(data && data.result) {
					$.messager.alert('信息', '删除成功！', 'info');
					$('#bannergrid').datagrid('reload');
				}
			}
		});
	}
	
	function clearselection() {
		$('#bannergrid').datagrid('clearSelections');
		$('#bannergrid').datagrid('clearChecked');
	}
//-->
</script>
<div style="width:1200px;border: 0">
<div id="tb">
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="shownews()">查看文章</a>
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="addNews()">添加文章</a>
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="createbanner()">
	<c:if test="${leveltype == 1 }">生成banner</c:if>
	<c:if test="${leveltype == 2 }">生成活动新闻</c:if>
	</a>
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="deletenews()">
	<c:if test="${leveltype == 1 }">删除banner</c:if>
	<c:if test="${leveltype == 2 }">删除活动新闻</c:if>
	</a>
</div>
<div id="bannergriddiv" style="height:615px;">
	<table class="easyui-datagrid" id="bannergrid" data-options="url:'querynews.do?levelt=${leveltype }', pagination:true, pageSize:10,
	remoteSort:true, toolbar:'#tb', fit:true, nowrap:false, checkOnSelect:false, selectOnCheck:false, singleSelect:true">
		<thead>
			<tr>
				<th data-options="field:'ck',checkbox:true"></th>
				<th data-options="field:'title', sortable:true" width="250">文章题目</th>
				<th data-options="field:'marks', sortable:true" width="250">备注</th>
				<th data-options="field:'url', sortable:true" width="800">网络地址</th>
				<th data-options="field:'path', sortable:true" width="600">主图片路径</th>
				<th data-options="field:'levelname', sortable:true" width="150">一级类别</th>
				<th data-options="field:'typename', sortable:true" width="150">二级类别</th>
			</tr>
		</thead>
	</table>

</div>
</div>