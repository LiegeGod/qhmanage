<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>

<!-- include libries(jQuery, bootstrap, fontawesome) -->
<link href="${ctx }/resources/css/news.css" rel="stylesheet"> 
<link href="${ctx }/resources/css/bootstrap.css" rel="stylesheet"> 
<script src="${ctx }/resources/js/bootstrap.js"></script> 
<link href="${ctx }/resources/css/font-awesome.css" rel="stylesheet">
 
<!-- include summernote css/js-->
<link href="${ctx }/resources/css/summernote.css" rel="stylesheet">
<script src="${ctx }/resources/js/summernote.min.js"></script>
 
<!--国际化 -->
<script src="${ctx }/resources/js/lang/summernote-zh-CN.js"></script>
<script type="text/javascript">
<!--
	$(function(){
		  $('#summernote').summernote({
              lang: 'zh-CN', // default: 'en-US'
              height: 300
          });
		  
		  $("#levelt").on("change", function() {
			  selectLChange();
		  });
		  
		  $(".submit-btn").on("click", function() {
			  $("#content").val($("#summernote").code());
			  $.ajax({
				  url: "savenews.do",
				  type: "POST",
				  data: $("#myForm").serialize(),
				  async: false,
				  success: function(data) {
					  if(data && data.result) {
						  var msg = "成功！";
						  var _that = parent;
						  parent.$.messager.confirm('信息', (data.result == 1 ? "录入" : "修改") + msg, function(r) {
							  var id = "addNews";
							  if(data.result == 2) {
								  id = "editNews";
							  }
							  parent.closeTab(id);
						  });
					  } else {
						  parent.$.messager.alert('错误', data.result, 'error');
					  }
				  }
			  });
		  });
		  
		  $(".clear-btn").on("click", function(){
			  clear();
		  });
	});
	
	function sendFiles(file) {
	    var data = new FormData();
	    data.append("file", file);
	    var pid = $("#pid").val();
	    data.append("pid", pid);
	    $.ajax({
	        url: "uploadImage.do",
	        data: data,
	        type: "POST",
	        cache: false,
	        contentType: false,
	        processData: false,
	        success: function(data) {
	        	if(data && data.url && data.url != "") {
	        		var base_path = "${ctx }";
	        		$('#summernote').summernote("editor.insertImage", base_path + data.url);
	        	}
	        }
	    });
	}
	
	function clear() {
		$("#title").val("");
		$("#type").val(1);
		$("#summernote").code("");
	}
	
	function selectLChange() {
		var $type = $("#type");
		var $level = $("#levelt");
		var value = "",
			childs = $level.children();
		for(var i=0; i<childs.length; i++) {
			if($level.val() == $(childs[i]).attr("value")) {
				value = $(childs[i]).attr("data-type")
			}
		}
		parent.$.messager.progress({
			title: 'Please waiting',
			msg: 'Loading data...'
		});
		$.ajax({
			url: "querynewstypebyl.do",
			type: "POST",
			data: {"value":value},
			async: false,
			success: function(data) {
				if(data && data.result == 1) {
					var _list = data.list;
					var str = "<select id=\"type\" name=\"type\" class=\"news-type-value form-control\"></select>";
					var $str = $(str);
					$str.append("<option value=\"\">请选择</option>");
					for(var i=0; i<_list.length; i++) {
						$str.append("<option value=\"" + _list[i].value + "\">" + _list[i].name + "</option>");
					}
					$type.after($str);
					$type.remove();
				}
				parent.$.messager.progress('close');
			}
		});
	}
//-->
</script>
<style type="text/css">
.form-news {
	width: 80%;
	margin: 0 auto;
}

.news-title {
	height: 70px;
}

.news-type {
	height: 100px;
	margin-bottom: 60px;
}

.news-title-label, .news-type-label {
	font-size: 18px;
	font-weight: bold;
}

.news-content {
	margin-top: 20px;
}
</style>

<form class="form-news form-horizontal" id="myForm">
<div class="news-title">
<label class="news-title-label" for="title">题目</label>
<div>
	<input id="title" name="title" class="news-title-value form-control" value="${vo.title }">
</div>
</div>
<div class="news-type">
<label class="news-type-label" for="title">一级类型</label>
<div>
	<select id="levelt" name="levelt" class="news-type-value form-control">
		<option value="">请选择</option>
		<c:forEach items="${ml }" var="m">
		<option value="${m.value }" data-type="${m.cid }">${m.name }</option>
		</c:forEach>
	</select>
</div>
<label class="news-type-label" for="title">二级类型</label>
<div class="change-type">
	<select id="type" name="type" class="news-type-value form-control">
		<option value="">请选择</option>
	</select>
</div>
</div>
<div class="news-content">
<label class="news-title-label">内容</label>
<div style="display: none;"><input type="hidden" id="cid" name="cid" value="${vo.cid }"></div>
<div style="display: none;"><input type="hidden" id="pid" name="pid" value="${pid }"></div>
<div style="display: none;"><input type="hidden" id="content" name="content"></div>
<div id="summernote">Hello Summernote</div>
</div>
<center>
<a class="btn btn-primary submit-btn" href="javascript:void(0);">提交</a>
<a class="btn btn-primary clear-btn" href="javascript:void(0);">重置</a>
</center>
</form>
<script type="text/javascript">
	$(function() {
		var $vo = "${vo }";
		if($vo != "" && $vo != "{}") {
			$("#summernote").code('${vo.content }');
			$("#levelt").val("${vo.levelt }");
			selectLChange();
			$("#type").val("${vo.type }");
		}
	});
</script>