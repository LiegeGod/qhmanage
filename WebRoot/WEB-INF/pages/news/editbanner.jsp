<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<link type="text/css" href="${ctx }/resources/css/banner.css" rel="stylesheet" />
<script src="${ctx }/resources/js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
	$(function() {
		var _file = null;
		
		$(".submit-btn").button().click(function() {
			submit();
		});
		
		function submit() {
			var _data = new FormData();
		    _data.append("file", _file);
		    var cid = $("#cid").val();
		    var marks = $("#marks").val();
		    _data.append("cid", cid);
		    _data.append("marks", marks);
			$.ajax({
				url: "savebanner.do",
				type: "POST",
				async: false,
				data: _data,
				cache: false,
		        contentType: false,
		        processData: false,
				success: function(data) {
					if(data && data.result) {
						if(data.result == 1) {
							var msg = "创建成功!";
							parent.$.messager.confirm('信息', msg, function(r) {
								var id = "createBanner";
								parent.$('#' + id).window('close');
							});
						} else {
							parent.$.messager.alert('错误', data.result, 'error');
						}
					}
				}
			});
		}
		
		$(".filePrew").on("change", function(e) {
			var file = e.target.files[0]||e.dataTransfer.files[0];
			_file = file;
			if(file) {
				 var reader = new FileReader();
				 reader.onload = function() {
					 $(".showpic").html("");
					 $(".showpic").append("<img src='"+this.result+"'/>");
				 };
			}
			reader.readAsDataURL(file);
		});
		
	});
	
//-->
</script>
<div>
	<form action="" id="myform" enctype="multipart/form-data">
		<table width="100%" class="form-table">
			<tr>
				<td align="center">
				<a class=btn_addPic href="javascript:void(0);"><span><em>+</em>添加图片</span>
				<input class="filePrew" title="支持jpg、jpeg、gif、png格式，文件小于5M" tabIndex="5" type="file" size="5" name="file">
				</a>
				</td>
			</tr>
			<tr>
				<td>
					<label>备注：</label>
					<div>
					<textarea rows="2" cols="50" id="marks" name=marks></textarea>
					</div>
				</td>
			</tr>
			<tr>
				<td align="left">
					<label>以下为主题图片：</label>
				</td>
			</tr>
			<tr>
				<td align="center">
				<div class="showpic" align="center"></div>
				</td>
			</tr>
		</table>
		<input type="hidden" id="cid" name="cid" value="${vo.cid }">
		<center class="center-btn">
		<a class="submit-btn" href="javascript:void(0);">提交</a>
		</center>
	</form>
</div>