<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<style type="text/css">
.title {
	font-size: 30px;
	font: bold;
	text-align: center;
}
</style>
<div>
	<div class="title">${vo.title }</div>
	${vo.content }
</div>