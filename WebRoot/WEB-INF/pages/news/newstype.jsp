<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<script type="text/javascript">
<!--
	function showwin(id, title, src){
		showWindow({ 
		title: title,
		id: id,
		src: src,
	    minimizable:false,
	    collapsible:false, 
	    modal:true,
	    width:document.body.clientWidth/3,
	    height:document.body.clientHeight/2,
	    onClose:function(){
	    	$('#newstypegrid').datagrid('reload');
	    }
		});
	}
	
	function addType() {
		showwin("addType", "录入类型", "${ctx }/news/addnewstype.do");
	}
	
	function editType() {
		var selectedrows = $('#newstypegrid').datagrid('getChecked');
		if(selectedrows.length > 1){
			$.messager.alert('信息', '只能选择一条记录。', 'info');
			clearselection();
	      	return;
		}
		if(selectedrows.length == 1) {
			var selectedrow = selectedrows[0];
			showwin("editType", "修改类型", "${ctx }/news/editnewstype.do?cid=" + selectedrow.cid);
		} else {
			$.messager.alert('信息', '请选择选择一条记录。', 'info');
			return;
		}
	}
	
	function deleteType() {
		var selectedrows = $('#newstypegrid').datagrid('getChecked');
		if(selectedrows.length < 1) {
			$.messager.alert('信息', '请选择选择一条记录。', 'info');
			return;
		}
		var cids = new Array();
		for(var i=0; i<selectedrows.length; i++) {
			cids.push(selectedrows[i].cid);
		}
		$.messager.confirm("信息", "确定删除该类型?", function(r) {
			if(r) {
				$.ajax({
					url: "deletenewstype.do",
					type: "POST",
					async: false,
					data: {cids:cids.join(",")},
					success: function(data) {
						if(data && data.result) {
							$.messager.alert('信息', '删除成功！', 'info');
							$('#newstypegrid').datagrid('reload');
						}
					}
				});
			}
		});
	}
	
	function clearselection() {
		$('#newstypegrid').datagrid('clearSelections');
		$('#newstypegrid').datagrid('clearChecked');
	}
//-->
</script>
<div style="width:800px;border: 0">
<div id="tb">
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="addType()">添加类型</a>
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="editType()">修改类型</a>
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="deleteType()">删除类型</a>
</div>
<div id="newstypegriddiv" style="height:615px;">
	<table class="easyui-datagrid" id="newstypegrid" data-options="url:'querynewstype.do', pagination:true, pageSize:10,
	remoteSort:true, toolbar:'#tb', fit:true, nowrap:false, checkOnSelect:false, selectOnCheck:false, singleSelect:true">
		<thead>
			<tr>
				<th data-options="field:'ck',checkbox:true"></th>
				<th data-options="field:'name', sortable:true" width="150">类型名称</th>
				<th data-options="field:'value', sortable:true, align:'right'" width="150">类型值</th>
				<th data-options="field:'level', sortable:true,
				formatter:function(value, rowData, rowIndex) {
				if(value==1) {
					return '一级类别';
				} else {
					return '二级类别';
				}
				}" width="150">类型等级</th>
			</tr>
		</thead>
	</table>

</div>
</div>