<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<script type="text/javascript">
<!--
	function showwin(id, title, src){
		parent.addTab(id, title, src, function() {
			$('#newsgrid').datagrid('reload');
		});
	}
	
	function shownews() {
		var selectedrows = $('#newsgrid').datagrid('getChecked');
		if(selectedrows.length > 1){
			$.messager.alert('信息', '只能选择一条记录。', 'info');
			clearselection();
	      	return;
		}
		if(selectedrows.length == 1) {
			var selectedrow = selectedrows[0];
			showwin("showNews", "查看文章", "${ctx }/news/shownews.do?cid=" + selectedrow.cid);
		} else {
			$.messager.alert('信息', '请选择选择一条记录。', 'info');
			return;
		}
	}
	
	function addNews(){
		showwin("addNews", "录入文章", "${ctx }/news/addnews.do");
	}
	
	function editNews(){
		var selectedrows = $('#newsgrid').datagrid('getChecked');
		if(selectedrows.length > 1){
			$.messager.alert('信息', '只能选择一条记录。', 'info');
			clearselection();
	      	return;
		}
		if(selectedrows.length == 1) {
			var selectedrow = selectedrows[0];
			showwin("editNews", "修改文章", "${ctx }/news/editnews.do?cid=" + selectedrow.cid);
		} else {
			$.messager.alert('信息', '请选择选择一条记录。', 'info');
			return;
		}
	}
	
	function deletenews() {
		var selectedrows = $('#newsgrid').datagrid('getChecked');
		if(selectedrows.length < 1) {
			$.messager.alert('信息', '请选择选择一条记录。', 'info');
			return;
		}
		var cids = new Array();
		for(var i=0; i<selectedrows.length; i++) {
			cids.push(selectedrows[i].cid);
		}
		$.messager.confirm("信息", "确定删除该文章?", function(r) {
			if(r) {
				$.ajax({
					url: "deletenews.do",
					type: "POST",
					async: false,
					data: {cids:cids.join(",")},
					success: function(data) {
						if(data && data.result) {
							$.messager.alert('信息', '删除成功！', 'info');
							$('#newsgrid').datagrid('reload');
						}
					}
				});
			}
		});
	}
	
	function clearselection() {
		$('#newsgrid').datagrid('clearSelections');
		$('#newsgrid').datagrid('clearChecked');
	}
//-->
</script>
<div style="width:1200px;border: 0">
<div id="tb">
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="shownews()">查看文章</a>
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onclick="addNews()">添加文章</a>
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-edit',plain:true" onclick="editNews()">修改文章</a>
	<a href='javascript:void(0)' class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onclick="deletenews()">删除文章</a>
</div>
<div id="newsgriddiv" style="height:615px;">
	<table class="easyui-datagrid" id="newsgrid" data-options="url:'querynews.do', pagination:true, pageSize:10,
	remoteSort:true, toolbar:'#tb', fit:true, nowrap:false, checkOnSelect:false, selectOnCheck:false, singleSelect:true">
		<thead>
			<tr>
				<th data-options="field:'ck',checkbox:true"></th>
				<th data-options="field:'title', sortable:true" width="250">文章题目</th>
				<th data-options="field:'marks', sortable:true" width="250">备注</th>
				<th data-options="field:'url', sortable:true" width="800">网络地址</th>
				<th data-options="field:'path', sortable:true" width="600">主图片路径</th>
				<th data-options="field:'levelname', sortable:true" width="150">一级类别</th>
				<th data-options="field:'typename', sortable:true" width="150">二级类别</th>
			</tr>
		</thead>
	</table>

</div>
</div>