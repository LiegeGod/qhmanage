<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ page language="java" import="java.util.*"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<c:set var="ctx" value="${pageContext.request.contextPath }"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>登录</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	
	<link type="text/css" href="${ctx }/resources/css/style.css" rel="stylesheet" />
	<!-- Le styles -->
	<link type="text/css" href="${ctx }/resources/css/jquery-ui-1.10.0.custom.css" rel="stylesheet" />

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<link rel="stylesheet" href="${ctx }/resources/themes/gray/easyui.css" type="text/css"></link>
	<link rel="stylesheet" href="${ctx }/resources/themes/icon.css" type="text/css"></link>
	<link rel="stylesheet" href="${ctx }/resources/themes/color.css" type="text/css"></link>
	<script src="${ctx }/resources/js/jquery-1.9.1.min.js" type="text/javascript"></script>
	<script src="${ctx }/resources/js/jquery.md5.js" type="text/javascript"></script>
	<script src="${ctx }/resources/js/jquery-ui-1.9.2.custom.min.js" type="text/javascript"></script>
	<script src="${ctx }/resources/js/default.js" type="text/javascript"></script>
	<style type="text/css">
	.system-title {
		font-size: 22px;
		font: bold;
		text-align: center;
	}
	
	.system-message {
		font-size: 14px;
		font: bold;
		color: red; 
		text-align: center;
		height: 50px;
		line-height: 50px;
	}
	
	#loginform {
		margin: 0 auto;
		width: 30%;
	}
	
	.text-name {
		text-align: center;
	}
	
	table tr {
		height: 50px;
		line-height: 50px;
	}
	</style>
	<script type="text/javascript">
	
		if(window != top){
			top.location.href = location.href;
		}
	
		$(function(){
			
			$(".submit-btn").button().click(function() {
				$.ajax({
					url: "${ctx }/public/login.do",
					type: "POST",
					data: {
						username:$("#username").val(),
						password:$.md5($("#password").val())
					},
					async: false,
					success: function(data) {
						if(data && data.result == 1) {
							window.location.href = "${ctx }";
						} else {
							$(".system-message").html(data.result);
						}
					}
				});
			});
			
			$(".clear-btn").button().click(function() {
				$("#username").val("");
				$("#password").val("");
			});
		});
	</script>
  </head>
  
  <body>
  	<div class="system-title">后台管理</div>
  	<div class="system-message"></div>
    <form action="" id="loginform" method="post">
	    <table border="0" cellpadding="0" cellspacing="0" width="100%">
	    	<tr>
	    		<td width="50%" class="text-name">
	    			<label>用户名：</label>
	    		</td>
	    		<td width="50%">
	    			<input type="text" id="username" name="username">
	    		</td>
	    	</tr>
	    	<tr>
	    		<td class="text-name">
	    			<label>密&nbsp;&nbsp;&nbsp;码：</label>
	    		</td>
	    		<td>
	    			<input type="password" id="password" name="password">
	    		</td>
	    	</tr>
	    	<tr>
	    		<td colspan="2">
	    			<center>
	    			<a class="submit-btn" href="javascript:void(0);">登录</a>
					<a class="clear-btn" href="javascript:void(0);">重置</a>
	    			</center>
	    		</td>
	    	</tr>
	    </table>
    </form>
  </body>
</html>
