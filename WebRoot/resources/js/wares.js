$(function() {
	$(".submit-btn").on("click", function() {
		var sellingdate = "";
		$("#dateinput").find("input").each(function(){
			var val = $(this).val();
			sellingdate += val + ",";
		});
		if(sellingdate != ""){
			sellingdate = sellingdate.substring(0, sellingdate.length - 1);
		}
		var _data = new FormData();
		if(files.length > 0) {
			for(var i=0; i<files.length; i++) {
		    	_data.append("file", files[i], files[i].name);
			}
		} else {
			var blob = new Blob([""], {type:"text/plain"});
			_data.append("file", blob, "");
		}
		$("#sellingTime").val(sellingdate);
		_data.append("sellingTime", sellingdate);
	    var id = $("#id").val();
	    _data.append("id", id);
	    var name = $("#name").val();
	    _data.append("name", name);
	    var beforeName = $("#beforeName").val();
	    _data.append("beforeName", beforeName);
	    var waresType = $("#waresType").val();
	    _data.append("waresType", waresType);
	    var waresKind = $("#waresKind").val();
	    _data.append("waresKind", waresKind);
	    var goldType = $("#goldType").val();
	    _data.append("goldType", goldType);
	    var gold = $("#gold").val();
	    _data.append("gold", gold);
	    var duration = $("#duration").val();
	    _data.append("duration", duration);
	    var durationUnit = $("#durationUnit").val();
	    _data.append("durationUnit", durationUnit);
	    var num = $("#num").val();
	    _data.append("num", num);
	    var randomFlag = $("#randomFlag").val();
	    _data.append("randomFlag", randomFlag);
	    var description = $("#description").val();
	    _data.append("description", description);
	    var beforeDescription = $("#beforeDescription").val();
	    _data.append("beforeDescription", beforeDescription);
	    var money = $("#money").val();
	    _data.append("money", money);
	    _data.append("arr", arr.join(";"));
		$.ajax({
			url: "saveWares.do",
			type: "POST",
			data: _data,
			async: false,
			cache: false,
	        contentType: false,
	        processData: false,
			success: function(data) {
			  	if(data && data.result) {
				  	var msg = "成功！";
				  	parent.$.messager.confirm('信息', (data.result == 1 ? "添加" : "修改") + msg, function(r) {
					  	var id = "addWares";
					  	if(data.result == 2) {
						  	id = "editWares";
					  	}
					  	parent.closeTab(id);
				  	});
			  	} else {
				  	parent.$.messager.alert('错误', data.result, 'error');
			  	}
		  	}
		});
  	});
	
	$(".add-btn").on("click", addDateInput);
	
	$(".filePrew").on("change", function(e) {
		var _files = e.target.files||e.dataTransfer.files;
		if(_files) {
			for(var i=0; i<_files.length; i++) {
				files.push(_files[i]);
			 	var reader = new FileReader();
			 	reader.onload = function() {
				 	$(".show-image-view").append("<div class=\"view\" index=\"n_" + (files.length - 1) + "\"><img src='"+this.result+"'/></div>");
				 	$(".view").on("click", deleteImage);
			 	};
				reader.readAsDataURL(_files[i]);
			}
		}
	});
	
	$(".clear-btn").on("click", function(){
		clear();
	});
	
	moneyOnChange();
	$("#waresKind").on("change", moneyOnChange);
	
	init();
	
});

var files = new Array();
var arr = new Array();

function init() {
	var sellingTimes = $("#sellingTime").val();
	if(sellingTimes && sellingTimes.length > 0) {
		createDateInput(sellingTimes);
	}
	var picture = $("#picture").val();
	if(picture && picture.length > 0) {
		setArr(picture);
	}
}

function moneyOnChange() {
	var waresKind = $("#waresKind").val();
	if(waresKind == 1) {
		$(".smoney").show();
		$("#money").show();
	} else {
		$(".smoney").hide();
		$("#money").hide();
	}
}

function clear() {
	$("#name").val("");
	$("#beforeName").val("");
	$("#waresType").val("1");
	$("#waresKind").val("0");
	$("#goldType").val("1");
	$("#gold").val("");
	$("#sellingTime").val("");
	$("#dateinput").html("");
	$("#duration").val("");
	$("#durationUnit").val("4");
	$("#num").val("");
	$("#randomFlag").val("0");
	$("#description").val("");
	$("#beforeDescription").val("");
	$("#money").val("");
}

function deleteImage() {
	var index_t = $(this).attr("index");
	var that = this;
	if(index_t != "") {
		var index = index_t.split("_");
		if(index.length > 0) {
			parent.$.messager.confirm('信息', "是否要删除该图片?", function(r) {
				if(r) {
					if(index[0] == "o") {
						arr.splice(index[1], 1);
					} else {
						files.splice(index[1], 1);
					}
					$(that).remove();
				}
			});
		}
	}
}

function createDateInput(val) {
	var div = $("#dateinput");
	if(val != "") {
		var vals = val.split(",");
		for(var i = 0;i<vals.length;i++) {
			var str = "<div class=\"date-div-left\"><input id=\"sellingTime" + i + "\" name=\"sellingTime" + i + "\" class=\"form-control selling-time\" value=\"" 
				+ vals[i] + "\" ></div>";
			div.append(str);
		}
	} else {
		var str = "<div class=\"date-div-left\"><input id=\"sellingTime0\" name=\"sellingTime0\" class=\"form-control selling-time\" ></div>";
		div.append(str);
	}
	div.find("input").datetimepicker();
}

function addDateInput() {
	var div = $("#dateinput");
	var div_input = div.find("input");
	var i = div_input.length;
	var str = "<div class=\"date-div-left\"><input id=\"sellingTime" + i + "\" name=\"sellingTime" + i + "\" class=\"form-control selling-time\" ></div>";
	div.append(str);
	div.find("input").datetimepicker();
}

function setArr(picture) {
	var div = $(".show-image-view");
	if(picture != "") {
		var vals = picture.split("(X)");
		for(var i = 0;i<vals.length;i++) {
			var val = vals[i];
			if(val != null && val != "null" 
				&& val != "" && val.indexOf("@@") != -1) {
				var val_s = val.split("@@");
				arr.push(val_s[0]);
				div.append("<div class=\"view\" index=\"o_"+i+"\"><img src='"+val_s[1]+"'/></div>");
			}
		}
		$(".view").on("click", deleteImage);
	}
}