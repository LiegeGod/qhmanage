$(function(){
	$('#summernote').summernote({
		lang: 'zh-CN', // default: 'en-US'
		height: 300
    });
	
	onlyInputNumber();
	
	$(".submit-btn").on("click", function() {
		$("#content").val($("#summernote").code());
			$.ajax({
				url: "savecompetition.do",
			  	type: "POST",
			  	data: $("#myForm").serialize(),
			  	async: false,
			  	success: function(data) {
				  	if(data && data.result) {
					  	var msg = "成功！";
					  	var _that = parent;
					  	parent.$.messager.confirm('信息', (data.result == 1 ? "录入" : "修改") + msg, function(r) {
						  	var id = "addCompetition";
						  	if(data.result == 2) {
							  	id = "editCompetition";
						  	}
						  	parent.closeTab(id);
					  	});
				  	} else {
					  	parent.$.messager.alert('错误', data.result, 'error');
				  	}
			  	}
		});
  	});
	  
	$(".clear-btn").on("click", function(){
		clear();
	});
	
	$(".rule-btn").on("click", function() {
		setRule();
	});
});

function clear() {
	$("#title").val("");
	$("#summernote").code("");
	$("#rule").val("");
}

function rule_yes_btn() {
	var form_value = $("#rule-form");
	var json = getFormJson(form_value);
	var length = $(".rule-table").find("tr").length;
	var new_json = {};
	new_json["isauto"] = json.isauto;
	var array = new Array();
	for(var i=1; i<length-3; i++) {
		var tmp = {};
		var sql1 = json["sql_1_"+i],
			sql2 = json["sql_2_"+i],
			goldtype = json["goldtype"+i],
			sendtype = json["sendtype"+i],
			name = json["name"+i],
			gold = json["gold"+i];
		if(name != "" && gold != "") {
			tmp["sql1"] = sql1;
			tmp["sql2"] = sql2;
			tmp["goldtype"] = goldtype;
			tmp["sendtype"] = sendtype;
			tmp["name"] = name;
			tmp["gold"] = gold;
			array.push(tmp);
		}
	}
	new_json["rule"] = array;
	var json_str = JSON.stringify(new_json);
	$("#rule").val(json_str);
	setContentCode();
}

function setRule() {
	var content = "<form id=\"rule-form\">";
		content += "<table class=\"rule-table\" border=\"0\" width=\"800px\">";
		content += "<tr><td colspan=\"6\"><label>选择派奖方式：</label>";
		content += "</td></tr>";
		content += "<tr><td colspan=\"3\" style=\"text-align: center;\">";
		content += "<input type=\"radio\" id=\"isauto\" name=\"isauto\" value=\"1\" checked>自动</td>";
		content += "<td colspan=\"3\" style=\"text-align: center;\">";
		content += "<input type=\"radio\" id=\"isnotauto\" name=\"isauto\" value=\"2\">手动";
		content += "</td></tr>";
		content += "<tr><td colspan=\"6\"><label>条件：</label>";
		content += "<a href=\"javascript:void(0);\" class=\"btn btn_normal add-btn\">添加</a>";
		content += "</td></tr>";
		content += "<tr><td colspan=\"2\" width=\"30%\" align=\"center\">范围</td>";
		content += "<td width=\"15%\" align=\"center\">胜率或排名定义</td>";
		content += "<td width=\"15%\" align=\"center\">派奖类别</td>";
		content += "<td width=\"15%\" align=\"center\">派奖方式</td>";
		content += "<td width=\"15%\" align=\"center\">奖励";
		content += "</td></tr>";
		content += "</table></form>";
	easyDialog.open({
		container : {
			header : '设置规则',
			content : content,
			yesFn : rule_yes_btn,
			noFn : true
		},
		fixed : false
	});
	btn_action();
	goldtypeChange();
	dialogInit();
	onlyInputNumber();
}

function analysingRule() {
	var rule = $("#rule").val();
	if(rule == "") {
		return {};
	}
	var _rule_json = $.parseJSON(rule);
	return _rule_json;
}

function setSQLInput(obj, index) {
	$(".sql" + index).html("");
	var val = $(obj).val();
	var str = "";
	if(val != "1") {
		str += "第";
	}
	str += "<input id=\"sql_1_"+index+"\" name=\"sql_1_"+index+"\" style=\"width: 50px;\">";
	if(val == "1") {
		str += "%~<input id=\"sql_2_"+index+"\" name=\"sql_2_"+index+"\" style=\"width: 50px;\">%";
	} else {
		str += "名";
	}
	$(".sql" + index).html(str);
}

function btn_action() {
	var index = 1;
	$(".add-btn").on("click", function() {
		var _gold_type = $("#goldtype").val();
		var _gold = "金币";
		if(_gold_type == "1") {
			_gold = "现金";
		}
		var str = "<tr><td colspan=\"2\" width=\"30%\" align=\"center\"><div class=\"sql"+index+"\"><input id=\"sql_1_"+index+"\" name=\"sql_1_"+index+"\" style=\"width: 50px;\">%~";
			str	+= "<input id=\"sql_2_"+index+"\" name=\"sql_2_"+index+"\" style=\"width: 50px;\">%</div>";
			str	+= "</td>";
			str += "<td width=\"15%\" align=\"center\"><input id=\"name"+index+"\" name=\"name"+index+"\"></td>";
			str += "<td width=\"15%\" align=\"center\"><select id=\"sendtype"+index+"\" name=\"sendtype"+index+"\" ";
			str += "onChange=\"setSQLInput(this, '"+index+"')\">";
			str += "<option value=\"1\">胜率赛</option>";
			str += "<option value=\"2\">排名赛</option>";
			str += "</select>";
			str	+= "</td>";
			str += "<td width=\"15%\" class=\"gold_type\"><select id=\"goldtype"+index+"\" name=\"goldtype"+index+"\">";
			str += "<option value=\"1\">现金</option>";
			str += "<option value=\"2\">金币</option>";
			str += "</select>";
			str	+= "</td>";
			str += "<td width=\"15%\" align=\"center\"><input id=\"gold"+index+"\" name=\"gold"+index+"\"></td>";
			str += "</tr>";
		$(".rule-table").append(str);
		index++;
	});
}

function goldtypeChange() {
	$("#goldtype").on("change", function() {
		var _gold_type = $(this).val();
		var _gold = "金币";
		if(_gold_type == "1") {
			_gold = "现金";
		}
		$(".gold_type").children("span").html(_gold);
	});
}

function dialogInit() {
	var json = analysingRule();
	var isauto = json.isauto,
		array = json.rule || [];
	$("input[name=isauto]").each(function() {
		var val = $(this).val();
		if(val == isauto) {
			$(this).attr("checked", true);
		}
	});
	for(var i=1; i<=array.length; i++) {
		$(".add-btn").click();
		$("#goldtype"+i).val(array[i-1].goldtype);
		$("#sendtype"+i).val(array[i-1].sendtype);
		setSQLInput($("#sendtype"+i), i);
		$("#name"+i).val(array[i-1].name);
		$("#gold"+i).val(array[i-1].gold);
		$("#sql_1_"+i).val(array[i-1].sql1);
		$("#sql_2_"+i).val(array[i-1].sql2);
	}
}

function setContentCode() {
	var code = $("#summernote").code();
	var $code = $("<div>" + code + "</div>"),
		$rule = $code.find("div.rule");
	var json = analysingRule();
	var isauto = json.isauto,
		array = json.rule || [];
	var str = "";
	if(array.length > 0) {
		str += "<table border=\"0\" width=\"90%\" class=\"table table-bordered\">";
		str += "<tr>";
		str += "<td width=\"30%\" align=\"center\">范围";
		str += "</td>";
		str += "<td width=\"15%\" align=\"center\">定义";
		str += "</td>";
		str += "<td width=\"15%\" align=\"center\">类别";
		str += "</td>";
		str += "<td width=\"15%\" align=\"center\">派奖方式";
		str += "</td>";
		str += "<td width=\"15%\" align=\"center\">金额";
		str += "</td>";
		str += "</tr>";
		for(var i=0; i<array.length; i++) {
			str += "<tr>";
			str += "<td width=\"30%\" align=\"center\">" + array[i].sql1;
			if(array[i].sql2 != undefined && array[i].sql2 != "undefined" 
					&& array[i].sql2 != null) {
				str += "~" + array[i].sql2;
			}
			str += "</td>";
			str += "<td width=\"15%\" align=\"center\">"+array[i].name;
			str += "</td>";
			str += "<td width=\"15%\" align=\"center\">"+(array[i].sendtype==1?"胜率赛":"排名赛");
			str += "</td>";
			str += "<td width=\"15%\" align=\"center\">"+(array[i].goldtype==1?"现金":"金币");
			str += "</td>";
			str += "<td width=\"15%\" align=\"center\">"+array[i].gold;
			str += "</td>";
			str += "</tr>";
		}
		str += "</table>";
	}
	if($rule.length > 0) {
		$rule.html(str);
		$("#summernote").code($code.html());
	} else {
		code += "<div class=\"rule\">" + str + "</div>";
		$("#summernote").code(code);
	}
}

function onlyInputNumber() {
	var old_value = "";
	$(".number").on("blur", function() {
		var _value = $(this).val();
		var temp = /^\d+?(\.{0, 1}\d)?$/;
		if(temp.test(_value)) {
			old_value = $(this).val();
		} else {
			$(this).val(old_value);
		}
	});
	
	$(".number").on("focus", function() {
		old_value = $(this).val();
	});
}