Date.prototype.format = function(format){
	if(!format || format == ""){
		format = "yyyy-MM-dd";
	}
    var o = {
    "M+" : this.getMonth()+1, //month
    "d+" : this.getDate(),    //day
    "h+" : this.getHours() % 12 == 0 ? 12 : this.getHours() % 12,   //hour
    "H+" : this.getHours(),   //hour
    "m+" : this.getMinutes(), //minute
    "s+" : this.getSeconds(), //second
    "q+" : Math.floor((this.getMonth()+3)/3),  //quarter
    "t+" : this.getHours() > 12 ? "pm" : "am",
    "S" : this.getMilliseconds() //millisecond
    };
    var tt_flag = false;
    var tt_over = false;
    if(/(y+)/.test(format)) format=format.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
    for(var k in o){
    	if(new RegExp("("+ k +")").test(format)){
    		if(k == "h+" && !tt_over){
    			tt_flag = true;
    		} else if(k == "t+"){
    			tt_flag = false;
    			tt_over = true;
    		}
    		format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length));
    	}
    }
    if(tt_flag){
    	format += " " + (this.getHours() > 12 ? "pm" : "am");
    }
    return format;
}