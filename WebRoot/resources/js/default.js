//高级查询的收起、展开功能
function query_collapse(morebtnId,className){
	var obj = $("."+className);
	var btn = $("#"+morebtnId);
	var isHidden = obj.is(":hidden");
	if(isHidden){
		obj.show();
		btn.attr('title','点击隐藏');
		btn.addClass('more_upbtn');
		btn.removeClass('more_downbtn');
	} else {
		btn.attr('title','点击显示更多');
		obj.hide();
		btn.addClass('more_downbtn');
		btn.removeClass('more_upbtn');
	}	
	
}

function showWindow(option){
	var option_default = {
		window_parent: option.window_parent ? option.window_parent : null,
        parent_div: option.parent_div ? option.parent_div : ".hidden_div_",
		clz: option.clz ? option.clz : "",
		id: option.id ? option.id : "",
		src: option.src ? option.src : "",
		title: option.title ? option.title : "",
		minimizable: option.minimizable ? option.minimizable : false,
		maximizable: option.maximizable ? option.maximizable : false,
		collapsible: option.collapsible ? option.collapsible : false,
		modal: option.modal ? option.modal : true,
		width: option.width ? option.width : document.body.clientWidth-150,
		height:option.height ? option.height : document.body.clientHeight-100,
		content: option.content ? option.content : "",
		onClose: option.onClose ? option.onClose : function(){}
	};
	if(option_default.window_parent){
		option.window_parent = null;
		option_default.window_parent.showWindow(option);
	} else {
		if(option_default.content == "" && option_default.src != ""){
			option_default.content = "<iframe scrolling='auto' frameborder='0' src='" + option_default.src + "' style='width:100%; height:99.5%'></iframe>";
		}
		if(option_default.content != ""){
			var exist_flag = false;
			var div_str = "<div></div>";
			var $div = $(div_str);
			if(option_default.clz != ""){
				$div.addClass(option_default.clz);
				var $clz = $(".window>." + option_default.clz);
			    if($clz.length > 0){
			    	$div = $clz;
			    	exist_flag = true;
			    }
			}
			if(option_default.id != ""){
				$div.attr("id", option_default.id);
				var $id = $(".window>#" + option_default.id);
		        if($id.length > 0){
		        	$div = $id;
		        	exist_flag = true;
		        }
			}
		    if(!exist_flag){
		    	$(option_default.parent_div).append($div);
		    }
		    $div.window({
		        title: option_default.title,
		        minimizable: option_default.minimizable,
		        maximizable: option_default.maximizable,
		        collapsible: option_default.collapsible,
		        modal: option_default.modal,
		        width: option_default.width,
		        height: option_default.height,
		        content: option_default.content,
		        onClose: option_default.onClose
		    });
		    return $div;
		}
	}
}

function dateFormat(value, pattern) {
	var result = "";
	if(value && value != ""){
		try {
			var val = value;
			if(typeof value == "string"){
				val = value.replace(/-/g, "/");
			}
			var date = new Date(val);
			result = date.format(pattern);
		} catch(e) {
			result = "";
		}
	}
	return result;
}

function getFormJson(form) {
	var map = {};
	var array = $(form).serializeArray();
	$.each(array, function () {
		if (map[this.name] !== undefined) {
			if (!map[this.name].push) {
				map[this.name] = [map[this.name]];
			}
			map[this.name].push(this.value || '');
		} else {
			map[this.name] = this.value || '';
		}
	});
	return map;
}